//
//  ForgotPasswordVC.swift
//  eCommerce Buyer
//
//  Created by Theappfathers on 06/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmit(_ sender: UIButton) {
        if isValidation(){
            ForgotPasswordWebservice()
        }
    }
    
    //MARK:- API WEBSERVICES
    
    func ForgotPasswordWebservice(){
        self.view.endEditing(false)
        let urlString = createUrlForWebservice(url: APIManager.FORGOT_PASSWORD)
        
        let param:Parameters = ["email":txtEmail.text!]
        
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .post, queryParameters: nil, bodyParameters: param, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:ForgotPasswordModel?
            responseData = ForgotPasswordModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.AskConfirmation(title: AlertTitle.Information, message: responseData?.message ?? "") { result in
                    if result{
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                
            }else{
                AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: responseData?.message ?? "Something wrong.", viewcontroller: self)

                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
            AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: AlertMessages.kmsgServerError, viewcontroller: self)

        }
    }
}
extension ForgotPasswordVC{
    func isValidation() -> Bool{
        if (txtEmail.text?.isEmpty)!{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankEmail, viewcontroller: self)
            return false
        }
        if !isValidEmail(testStr: txtEmail.text!){
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgValidEmail, viewcontroller: self)
            return false
        }
        return true
    }
}
