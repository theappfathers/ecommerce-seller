//
//  RegisterVC.swift
//  eCommerce Buyer
//
//  Created by Theappfathers on 03/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire

class RegisterVC: UIViewController {
    
    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtBrand: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnTermsCondition: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblImageName: UILabel!
    
    
    var strImageFile = ""
    var imgProduct = UIImage()
    var mediaPickerhelper : MediaPickerHelper?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgProduct = #imageLiteral(resourceName: "Logo-Zeila")
        self.strImageFile = "1"

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignUp(_ sender: UIButton) {
        if isValidation(){
            RegistrationWebservice()
        }
    }
    @IBAction func btnChooseFile(_ sender: UIButton) {
        mediaPickerhelper = MediaPickerHelper(viewController: self, isForVideo : false, imageCallback: {
            image in
            
            let size = CGSize(width: 150, height: 150)
            let imagea = image?.crop(to: size)
            self.lblImageName.text = "Image.png"
            self.imgProduct = imagea!
            self.strImageFile = "1"
            self.imgProfile.image = imagea
            
        }, videoCallback: nil)
        
        mediaPickerhelper?.titleForActionSheet = "Select option for upload image"
        mediaPickerhelper?.titleForCameraButton = "Camera"
        mediaPickerhelper?.titleForPhotosButton = "Photos"
        mediaPickerhelper?.showPhotoSourceSelection()        
    }
    @IBAction func btnTermsCondition(_ sender: UIButton) {
        if sender.isSelected{
            btnTermsCondition.isSelected = false
//            imgTerms.image = UIImage(imageLiteralResourceName: "circle")
        }else{
            btnTermsCondition.isSelected = true
//            imgTerms.image = UIImage(imageLiteralResourceName: "circle-fill")
        }
    }
    
    //MARK:- API WEBSERVICES
    func RegistrationWebservice(){
        self.view.endEditing(false)
        
        let urlString = createUrlForWebservice(url: APIManager.REGISTRATION_URL)
        let DeviceType = UIDevice.modelName
        let UDID = UIDevice.current.identifierForVendor!.uuidString
        
        let param:Parameters = ["role":"seller","company_name":txtCompanyName.text!,"brand":txtBrand.text!,"email":txtEmail.text!,"password":txtPassword.text!,"c_password":txtConfirmPassword.text!,"device_id":UDID,"term_condition":"1","device_token":"123","device_type":DeviceType]
        
        print(param)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithMultipartDataURL(urlString, method: .post, imgProfile: imgProduct, imgName: "logo", queryParameters: nil, bodyParameters: param, headers: headers, completionHandler: { (response) in
            
            indicator.shared.hideIndicator()
            var responseData:Register_ResponseModel?
            responseData = Register_ResponseModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.AskConfirmation(title: AlertTitle.Information, message: responseData?.message ?? "") { result in
                    if result{
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                //                addBoolToUserDefaults(pBool: true, pForKey: "session")
                //                let nextVC = STORYBOARD.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                //                self.navigationController?.pushViewController(nextVC, animated: true)
            }else{
                AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: responseData?.message ?? "Something wrong.", viewcontroller: self)
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
            AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: AlertMessages.kmsgServerError, viewcontroller: self)
        }
    }
}
extension RegisterVC{
    func isValidation() -> Bool{
        
        if (txtCompanyName.text?.isEmpty)!{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankCompanyName, viewcontroller: self)
            
            return false
        }
        if (txtEmail.text?.isEmpty)!{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankEmail, viewcontroller: self)
            
            return false
        }
        if !isValidEmail(testStr: txtEmail.text!){
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgValidEmail, viewcontroller: self)
            
            return false
        }
        if (txtBrand.text?.isEmpty)! {
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankBrand, viewcontroller: self)
            return false
        }
        if (txtPassword.text?.isEmpty)! {
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankPassword, viewcontroller: self)
            return false
        }
        if (txtConfirmPassword.text?.isEmpty)! {
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgConfirmPassword, viewcontroller: self)
            
            return false
        }
        if txtPassword.text != txtConfirmPassword.text{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgPasswordNotMatch, viewcontroller: self)
            return false
        }
        if strImageFile == ""{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgChooseFile, viewcontroller: self)
            return false
        }
        if btnTermsCondition.isSelected == false{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgTermsAndCondition, viewcontroller: self)
            return false
        }
        
        return true
    }
}
