//
//  LoginVC.swift
//  eCommerce Buyer
//
//  Created by Theappfathers on 03/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire


class LoginVC: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

//        txtEmail.text = "a@a.com"
//        txtPassword.text = "123"

    }
    
    @IBAction func btnSignIn(_ sender: UIButton) {
        if isValidation(){
//            let nextVC = STORYBOARD.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
//            self.navigationController?.pushViewController(nextVC, animated: true)

            LoginWebservice()
        }

//        let nextVC = STORYBOARD.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
//        self.navigationController?.pushViewController(nextVC, animated: true)

    }
    @IBAction func btnRegister(_ sender: UIButton) {
        let nextVC = STORYBOARD.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    @IBAction func btnForgotPassword(_ sender: UIButton) {
        let nextVC = STORYBOARD.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }

    //MARK:- API WEBSERVICES
    func LoginWebservice(){
        self.view.endEditing(false)

        let urlString = createUrlForWebservice(url: APIManager.LOGIN_URL)

        let param:Parameters = ["email":txtEmail.text!,"password":txtPassword.text!]
        
        print(param)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .post, queryParameters: nil, bodyParameters: param, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:LoginDataModel?
            responseData = LoginDataModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                
                AppData.shared.saveUserInfo((responseData?.data)!)

                addBoolToUserDefaults(pBool: true, pForKey: "session")
                let nextVC = STORYBOARD.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                self.navigationController?.pushViewController(nextVC, animated: true)
            }else{
                AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: responseData?.message ?? "Something wrong.", viewcontroller: self)
                indicator.shared.hideIndicator()
            }
            
        }) { (error) in
            indicator.shared.hideIndicator()
            AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: AlertMessages.kmsgServerError, viewcontroller: self)
        }
    }
}

extension LoginVC{
    func isValidation() -> Bool{
        if (txtEmail.text?.isEmpty)!{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankEmail, viewcontroller: self)

            return false
        }
        if !isValidEmail(testStr: txtEmail.text!){
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgValidEmail, viewcontroller: self)

            return false
        }
        if (txtPassword.text?.isEmpty)! {
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankPassword, viewcontroller: self)
            return false
        }
        return true
    }
}
