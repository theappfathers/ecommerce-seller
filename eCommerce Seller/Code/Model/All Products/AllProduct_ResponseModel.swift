//
//  AllProduct_ResponseModel.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 22/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import Foundation
import ObjectMapper

class AllProduct_ResponseModel:Mappable{
    
    var success               : Bool?
    var message               : String?
    var code                  : Int?
    var data                  : [AllProductDataModel] = []

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success                 <- map["success"]
        code                    <- map["code"]
        message                 <- map["message"]
        data                    <- map["data"]
    }
}

class AllProductDataModel:Mappable{
    
    var id                      : Int?
    var name                    : String?
    var title                   : String?
    var code                    : String?
    var image                   : String?
    var description             : String?
    var price                   : Int?
    var qty                     : String?
    var style                   : String?
    var user_id                 : String?
    var type                    : [CommonCategoryDataList] = []
    var color                   : [CommonCategoryDataList] = []
    var size                    : [CommonCategoryDataList] = []
    var category                : [CommonCategoryDataList] = []

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        title                   <- map["title"]
        code                    <- map["code"]
        image                   <- map["image"]
        description             <- map["description"]
        price                   <- map["price"]
        qty                     <- map["qty"]
        style                   <- map["style"]
        user_id                 <- map["user_id"]
        type                    <- map["type"]
        color                   <- map["color"]
        size                    <- map["size"]
        category                <- map["category"]

    }
}
