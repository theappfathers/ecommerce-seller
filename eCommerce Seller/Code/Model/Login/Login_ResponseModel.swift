//
//  Login_ResponseModel.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 16/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginDataModel:Mappable{
    
    var success               : Bool?
    var data                  : LoginDetailsDataModel?
    var message               : String?
    var code                  : Int?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success                 <- map["success"]
        data                    <- map["data"]
        code                    <- map["code"]
        message                 <- map["message"]
    }
}

class LoginDetailsDataModel:NSObject, Mappable, NSCoding{
    
    var id                      : Int?
//    var name                    : String?
    var company_name            : String?
    var brand                   : String?
    var logo                    : String?
    var email                   : String?
//    var email_verified_at       : String?
    var term_condition          : String?
    var device_id               : String?
    var device_token            : String?
    var device_type             : String?
    var user_role               : String?

    required init?(map: Map) {
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
//        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.company_name = aDecoder.decodeObject(forKey: "company_name") as? String
        self.brand = aDecoder.decodeObject(forKey: "brand") as? String
        self.logo = aDecoder.decodeObject(forKey: "logo") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
//        self.email_verified_at = aDecoder.decodeObject(forKey: "email_verified_at") as? String
        self.term_condition = aDecoder.decodeObject(forKey: "term_condition") as? String
        self.device_id = aDecoder.decodeObject(forKey: "device_id") as? String
        self.device_token = aDecoder.decodeObject(forKey: "device_token") as? String
        self.device_type = aDecoder.decodeObject(forKey: "device_type") as? String
        self.user_role = aDecoder.decodeObject(forKey: "user_role") as? String
    }

    func encode(with aCoder: NSCoder){
        aCoder.encode(id, forKey: "id")
//        aCoder.encode(name, forKey: "name")
        aCoder.encode(company_name, forKey: "company_name")
        aCoder.encode(brand, forKey: "brand")
        aCoder.encode(logo, forKey: "logo")
        aCoder.encode(email, forKey: "email")
//        aCoder.encode(email_verified_at, forKey: "email_verified_at")
        aCoder.encode(term_condition, forKey: "term_condition")
        aCoder.encode(device_id, forKey: "device_id")
        aCoder.encode(device_token, forKey: "device_token")
        aCoder.encode(device_type, forKey: "device_type")
        aCoder.encode(user_role, forKey: "user_role")
    }

    func mapping(map: Map) {
        id                      <- map["id"]
//        name                    <- map["name"]
        company_name            <- map["company_name"]
        brand                   <- map["brand"]
        logo                    <- map["logo"]
        email                   <- map["email"]
//        email_verified_at       <- map["email_verified_at"]
        term_condition          <- map["term_condition"]
        device_id               <- map["device_id"]
        device_token            <- map["device_token"]
        device_type             <- map["device_type"]
        user_role               <- map["user_role"]
    }
}
