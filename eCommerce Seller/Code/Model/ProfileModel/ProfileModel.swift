//
//  ProfileModel.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 02/06/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileModel:Mappable{
    
    var success               : Bool?
    var message               : String?
    var code                  : Int?
    var data                  : ProfileDataModel?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success                 <- map["success"]
        code                    <- map["code"]
        message                 <- map["message"]
        data                    <- map["data"]
    }
}
class ProfileDataModel:Mappable{
    
    var id                    : Int?
    var name                  : String?
    var company_name          : String?
    var email                 : String?
    var image                 : String?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id                     <- map["id"]
        name                   <- map["name"]
        company_name           <- map["company_name"]
        email                  <- map["email"]
        image                  <- map["image"]
    }
}

class ShippingAddressModel:Mappable{
    
    var success               : Bool?
    var message               : String?
    var code                  : Int?
    var data                  : [ShippingAddressDataModel]?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success                 <- map["success"]
        code                    <- map["code"]
        message                 <- map["message"]
        data                    <- map["data"]
    }
}

class ShippingAddressDataModel:Mappable{
    
    var id                    : Int?
    var user_id               : Int?
    var country               : String?
    var city                  : String?
    var address               : String?
    var postcode              : String?
    var phone_number          : String?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id                    <- map["id"]
        user_id               <- map["user_id"]
        country               <- map["country"]
        city                  <- map["city"]
        address               <- map["address"]
        postcode              <- map["postcode"]
        phone_number          <- map["phone_number"]
    }
}
