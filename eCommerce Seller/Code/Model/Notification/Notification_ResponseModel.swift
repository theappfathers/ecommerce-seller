//
//  Notification_ResponseModel.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 18/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import Foundation
import ObjectMapper

class Notification_ResponseModel:Mappable{
    
    var success               : Bool?
    var code                  : Int?
    var message               : String?
    var data                  : [NotificationDataModel] = []

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success               <- map["success"]
        code                  <- map["code"]
        message               <- map["message"]
        data                  <- map["data"]
    }
}
class NotificationDataModel:Mappable{
    
    var id                  : Int?
    var name                : String?
    var description         : String?
    var created_at          : String?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        name                <- map["name"]
        description         <- map["description"]
        created_at          <- map["created_at"]
    }
}
