//
//  CommonModel.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 19/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import Foundation
import ObjectMapper

class CommonModel:Mappable{
    
    var success               : Bool?
    var message               : String?
    var code                  : Int?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success                 <- map["success"]
        code                    <- map["code"]
        message                 <- map["message"]
    }
}

//Category/size/color
class CommonCategoryDataModel:Mappable{
    
    var success               : Bool?
    var message               : String?
    var code                  : Int?
    var data                  : CommonExtraDataList?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success                 <- map["success"]
        code                    <- map["code"]
        message                 <- map["message"]
        data                    <- map["data"]
    }
}


class CommonExtraDataList:Mappable{
    
    var color                   : [CommonCategoryDataList]?
    var size                    : [CommonCategoryDataList]?
    var category                : [CommonCategoryDataList]?
    var type                    : [CommonCategoryDataList]?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        color                   <- map["color"]
        size                    <- map["size"]
        category                <- map["category"]
        type                    <- map["type"]
    }
}


class CommonCategoryDataList:Mappable{
    
    var id                    : Int?
    var name                  : String?
    var isSelected            : Bool = false

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        isSelected              <- map["isSelected"]
    }
}
class CategoryDataModel:Mappable{
    
    var success               : Bool?
    var message               : String?
    var code                  : Int?
    var data                  : [CategoryDataList] = []

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success                 <- map["success"]
        code                    <- map["code"]
        message                 <- map["message"]
        data                    <- map["data"]
    }
}

class CategoryDataList:Mappable{
    
    var id                    : Int?
    var name                  : String?
    var image                 : String?
    var isSelected            : Bool = false
    var sub_category          : [SubCategoryDataList] = []

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        image                   <- map["image"]
        isSelected              <- map["isSelected"]
        sub_category            <- map["sub_category"]
    }
}
class SubCategoryDataList:Mappable{
    
    var id                    : Int?
    var parent_id             : Int?
    var type_id               : Int?
    var name                  : String?
    var image                 : String?
    var isSelected            : Bool = false

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        parent_id               <- map["parent_id"]
        type_id                 <- map["parent_id"]
        name                    <- map["name"]
        image                   <- map["image"]
        isSelected              <- map["isSelected"]
    }
}
