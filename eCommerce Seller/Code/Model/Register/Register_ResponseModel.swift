//
//  Register_ResponseModel.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 17/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import Foundation
import ObjectMapper

class Register_ResponseModel:Mappable{
    
    var success               : Bool?
    var code                  : Int?
    var message               : String?
    var data                  : RegisterDataModel?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success               <- map["success"]
        code                  <- map["code"]
        message               <- map["message"]
        data                  <- map["data"]
    }
}
class RegisterDataModel:Mappable{
    
    var company_name            : String?
    var brand                   : String?
    var logo                    : String?
    var user_id                 : Int?
    var email                   : String?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        company_name             <- map["company_name"]
        brand                    <- map["brand"]
        logo                     <- map["logo"]
        user_id                  <- map["user_id"]
        email                    <- map["email"]
    }
}
