//
//  ForgotPassword_ResponseModel.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 16/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import Foundation
import ObjectMapper

class ForgotPasswordModel:Mappable{
    
    var success               : Bool?
    var code                  : Int?
    var message               : String?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success               <- map["success"]
        code                  <- map["code"]
        message               <- map["message"]
    }
}
