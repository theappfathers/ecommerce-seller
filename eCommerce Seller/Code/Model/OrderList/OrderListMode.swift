//
//  OrderListMode.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 15/05/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import Foundation
import ObjectMapper

class OrderListMode:Mappable{
    
    var success               : Bool?
    var code                  : Int?
    var message               : String?
    var data                  : [OrderListDataMode] = []

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success               <- map["success"]
        code                  <- map["code"]
        message               <- map["message"]
        data                  <- map["data"]
    }
}
class OrderListDataMode:Mappable{
    
    var id                  : Int?
    var name                : String?
    var description         : String?
    var created_at          : String?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        name                <- map["name"]
        description         <- map["description"]
        created_at          <- map["created_at"]
    }
}
