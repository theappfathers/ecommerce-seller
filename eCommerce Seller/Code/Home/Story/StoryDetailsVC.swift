//
//  StoryDetailsVC.swift
//  eCommerce Buyer
//
//  Created by Theappfathers on 10/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit

class StoryDetailsVC: UIViewController {

    @IBOutlet weak var imgCompanyImage: UIImageView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStoryTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var imgStoryImage: UIImageView!
    
    var objStoryDetails:StoryDataModel?
    var objCompanyDetails:CompanyDetailsDataModel?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        SetStoryDetails()
    }

    func SetStoryDetails(){

        lblCompanyName.text = objCompanyDetails?.company_name

        let strCompany = objCompanyDetails?.logo ?? ""
        imgCompanyImage.sd_setImage(with: URL.init(string: strCompany), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
        })

        lblStoryTitle.text = objStoryDetails?.name
        lblAmount.text = "$\(objStoryDetails?.price ?? 0)"
        let strImgUrl = objStoryDetails?.image ?? ""
        imgStoryImage.setShowActivityIndicator(true)
        imgStoryImage.setIndicatorStyle(.gray)
        imgStoryImage.sd_setImage(with: URL.init(string: strImgUrl), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
        })

    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
