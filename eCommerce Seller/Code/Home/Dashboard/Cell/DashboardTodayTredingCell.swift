//
//  DashboardTodayTredingCell.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 27/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit

class DashboardTodayTredingCell: UICollectionViewCell {
    
    static var userIdentifier = "DashboardTodayTredingCell"

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!

}
