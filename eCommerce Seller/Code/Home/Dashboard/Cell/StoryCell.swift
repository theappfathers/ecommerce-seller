//
//  StoryCell.swift
//  eCommerce Buyer
//
//  Created by Theappfathers on 09/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit

class StoryCell: UICollectionViewCell {

    static var userIdentifier = "StoryCell"
    
    @IBOutlet weak var imgCompany: UIImageView!
    @IBOutlet weak var imgAddStory: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var vwCardView: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgCompany.layer.masksToBounds = true
        self.vwCardView.clipsToBounds = true

    }
    override var bounds: CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.setCircularImageView()
        }
    }

    func setCircularImageView() {
        self.imgCompany.layer.cornerRadius = CGFloat(roundf(Float(self.imgCompany.frame.height / 2.0)))
        self.vwCardView.layer.cornerRadius = CGFloat(roundf(Float(self.vwCardView.frame.height / 2.0)))
    }
}
