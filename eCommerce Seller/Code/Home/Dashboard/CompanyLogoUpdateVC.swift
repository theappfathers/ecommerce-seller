//
//  CompanyLogoUpdateVC.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 12/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire
import Photos

class CompanyLogoUpdateVC: UIViewController {

    @IBOutlet weak var lblBrandLogoName: UILabel!
    @IBOutlet weak var imgBrandLogo: UIImageView!

//    var imgBrandLogo = UIImage()
    var mediaPickerhelper : MediaPickerHelper?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnChooseFile(_ sender: UIButton) {
        mediaPickerhelper = MediaPickerHelper(viewController: self, isForVideo : false, imageCallback: {
            image in
//            self.imgBrandLogo.image = (image?.resized(withPercentage: 0.3))!
            let size = CGSize(width: 100, height: 100)
            let imagea = image?.crop(to: size)
            self.imgBrandLogo.image = imagea

        }, videoCallback: nil)
        
        mediaPickerhelper?.titleForActionSheet = "Select option for upload image"
        mediaPickerhelper?.titleForCameraButton = "Camera"
        mediaPickerhelper?.titleForPhotosButton = "Photos"
        mediaPickerhelper?.showPhotoSourceSelection()
    }
    @IBAction func btnUpdate(_ sender: UIButton) {
        UpdateBrandLogoWebservice()
    }
    
    //MARK:- API WEBSERVICES
    func UpdateBrandLogoWebservice(){
        self.view.endEditing(false)

        let urlString = createUrlForWebservice(url: APIManager.LOGO_UPDATE_URL)
        var stUserID = 0
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }

        let param:Parameters = ["user_id":"\(stUserID)"]
        
        print(param)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithMultipartDataURL(urlString, method: .post, imgProfile: imgBrandLogo.image, imgName: "logo", queryParameters: nil, bodyParameters: param, headers: headers, completionHandler: { (response) in

            indicator.shared.hideIndicator()
            var responseData:CommonModel?
            responseData = CommonModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.AskConfirmation(title: AlertTitle.Information, message: responseData?.message ?? "") { result in
                    if result{
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }else{
                AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: responseData?.message ?? "Something wrong.", viewcontroller: self)
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
            AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: AlertMessages.kmsgServerError, viewcontroller: self)
        }
    }
}
