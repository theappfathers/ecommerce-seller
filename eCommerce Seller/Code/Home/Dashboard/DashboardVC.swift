//
//  DashboardVC.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 12/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire

class DashboardVC: UIViewController {

    @IBOutlet weak var collStory: UICollectionView!
    @IBOutlet weak var collTreding: UICollectionView!

    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var imgCompany: UIImageView!
    
    @IBOutlet weak var btnForYou: UIButton!
    @IBOutlet weak var btnTodayInTrend: UIButton!
    @IBOutlet weak var btnFollowing: UIButton!

    
    var arrStory = [StoryDataModel]()
    
    var arrTodayTreding = [TodayTrendProductModel]()
    var arrForYouCompany = [TodayTrendProductModel]()
    var arrFollowingCompany = [TodayTrendProductModel]()

    var objCompanyData:CompanyDetailsDataModel?
    var isSelectionType = "2"

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collStory.dataSource = self
        self.collStory.delegate = self
        self.collStory.reloadData()

        
        self.collStory.register(UINib.init(nibName: StoryCell.userIdentifier, bundle: nil), forCellWithReuseIdentifier: StoryCell.userIdentifier)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        GetDashboardWebservice()

        if let flowLayout = self.collStory.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
            flowLayout.minimumInteritemSpacing = 0
            self.collStory.reloadData()
        }
        self.collStory.reloadData()
     
        if let flowLayout = self.collTreding.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            flowLayout.scrollDirection = .vertical
            flowLayout.minimumLineSpacing = 0
            flowLayout.minimumInteritemSpacing = 0
            self.collTreding.reloadData()
        }
        self.collTreding.reloadData()

    }
    func SetCompanyDetails(objData:CompanyDetailsDataModel){
        lblCompanyName.text = objData.company_name
//        lblHeaderTitle.text = objData.company_name
        
        let strImgUrl = objData.logo ?? ""
        imgCompany.setShowActivityIndicator(true)
        imgCompany.setIndicatorStyle(.gray)
        imgCompany.sd_setImage(with: URL.init(string: strImgUrl), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
        })

    }

    @IBAction func btnForYou(_ sender: UIButton) {
        btnForYou.backgroundColor = hexStringToUIColor(hex: CommonColor.darkBlue)
        btnForYou.setTitleColor(.white, for: .normal)

        btnTodayInTrend.backgroundColor = .clear
        btnFollowing.backgroundColor = .clear
        
        btnTodayInTrend.setTitleColor(.gray, for: .normal)
        btnFollowing.setTitleColor(.gray, for: .normal)
        isSelectionType = "1"
        
        if arrForYouCompany.count == 0{
            collTreding.setEmptyMessage("No Data found")
        }else{
            collTreding.restore()
        }
        
        collTreding.reloadData()
    }
    @IBAction func btnTodayInTrend(_ sender: UIButton) {
        btnTodayInTrend.backgroundColor = hexStringToUIColor(hex: CommonColor.darkBlue)
        btnTodayInTrend.setTitleColor(.white, for: .normal)

        btnForYou.backgroundColor = .clear
        btnFollowing.backgroundColor = .clear
        
        btnForYou.setTitleColor(.gray, for: .normal)
        btnFollowing.setTitleColor(.gray, for: .normal)
        isSelectionType = "2"
        if arrTodayTreding.count == 0{
            collTreding.setEmptyMessage("No Data found")
        }else{
            collTreding.restore()
        }
        collTreding.reloadData()
    }
    @IBAction func btnFollowing(_ sender: UIButton) {
        btnFollowing.backgroundColor = hexStringToUIColor(hex: CommonColor.darkBlue)
        btnFollowing.setTitleColor(.white, for: .normal)

        btnTodayInTrend.backgroundColor = .clear
        btnForYou.backgroundColor = .clear
        
        btnTodayInTrend.setTitleColor(.gray, for: .normal)
        btnForYou.setTitleColor(.gray, for: .normal)
        isSelectionType = "3"
        if arrFollowingCompany.count == 0{
            collTreding.setEmptyMessage("No Data found")
        }else{
            collTreding.restore()
        }

        collTreding.reloadData()
    }

    
    @IBAction func btnSettings(_ sender: UIButton) {
        let next = STORYBOARD.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(next, animated: true)

//        self.AskConfirmation(title: AlertTitle.Information, message: "Are you sure you want to logout?") { result in
//            if result{
//                self.navigationController?.pushViewController(next, animated: true)
//            }
//        }
    }
    @IBAction func btnAddBrandLogo(_ sender: UIButton) {
        let next = STORYBOARD.instantiateViewController(withIdentifier: "CompanyLogoUpdateVC") as! CompanyLogoUpdateVC
        self.navigationController?.pushViewController(next, animated: true)
    }

    func GetDashboardWebservice(){
        self.view.endEditing(false)
        var stUserID = 0
        
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }
        let urlString = createUrlForWebservice(url: APIManager.GET_DASHBOARD_URL) + "/\(stUserID)"
        
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .get, queryParameters: nil, bodyParameters: nil, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:getAllDashboard_ResponseModel?
            responseData = getAllDashboard_ResponseModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                
                self.arrStory = responseData?.data?.story ?? []
                self.arrTodayTreding = responseData?.data?.todayTrendProduct ?? []
                self.objCompanyData = responseData?.data?.company
                self.SetCompanyDetails(objData:(responseData?.data!.company)!)
                self.collStory.reloadData()
                self.collTreding.reloadData()
                if self.arrTodayTreding.count == 0{
                    self.collTreding.setEmptyMessage("No Data found")
                }else{
                    self.collTreding.restore()
                }
            }else{
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
        }
    }
}
extension DashboardVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collStory{
            return arrStory.count + 1
        }else{
            if isSelectionType == "1"{
                return arrForYouCompany.count
            }else if isSelectionType == "2"{
                return arrTodayTreding.count
            }else{
                return arrFollowingCompany.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collStory{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoryCell.userIdentifier, for: indexPath) as! StoryCell
            
            if indexPath.row == 0{
                cell.lblName.text = "New Story"
                cell.imgCompany.image = nil
                cell.imgCompany.backgroundColor = hexStringToUIColor(hex: "1D518D")
                cell.imgAddStory.isHidden = false
            }else{
                let objData = arrStory[indexPath.row - 1]

                cell.imgCompany.backgroundColor = .white
                cell.lblName.text = objData.name
                cell.imgAddStory.isHidden = true
                let strImgUrl = objData.image ?? ""
                
                cell.imgCompany.setShowActivityIndicator(true)
                cell.imgCompany.setIndicatorStyle(.gray)
                cell.imgCompany.sd_setImage(with: URL.init(string: strImgUrl), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
                })
            }
            cell.layoutIfNeeded()
            cell.layoutSubviews()
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DashboardTodayTredingCell.userIdentifier, for: indexPath) as! DashboardTodayTredingCell
            
            let objData = arrTodayTreding[indexPath.row]
            
            cell.lblName.text = objData.name
            cell.lblPrice.text = "$\(objData.price ?? 0)"
            let strImgUrl = objData.image ?? ""
            cell.imgProduct.setShowActivityIndicator(true)
            cell.imgProduct.setIndicatorStyle(.gray)
            cell.imgProduct.sd_setImage(with: URL.init(string: strImgUrl), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
            })
            
            cell.layoutIfNeeded()
            cell.layoutSubviews()
            
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collStory{
            let width = (self.collStory.frame.size.height - 20)
            let height = width * 1.1 //ratio
            return CGSize(width: width, height: height)
        }else{
            let width = (self.collTreding.frame.size.width - 15) / 2.1
            let height = width * 1.5 //ratio
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collStory{
            if indexPath.row == 0{
                let nextVC = STORYBOARD.instantiateViewController(withIdentifier: "AddStoryVC") as! AddStoryVC
                self.navigationController?.pushViewController(nextVC, animated: true)
            }else{
                let objData = arrStory[indexPath.row - 1]

                let nextVC = STORYBOARD.instantiateViewController(withIdentifier: "StoryDetailsVC") as! StoryDetailsVC
                nextVC.objStoryDetails = objData
                nextVC.objCompanyDetails = objCompanyData
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
        }else{
            let nextVC = STORYBOARD.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
            nextVC.objProductDetails = arrTodayTreding[indexPath.row]
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
}
