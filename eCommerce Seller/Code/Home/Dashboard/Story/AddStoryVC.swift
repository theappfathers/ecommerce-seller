//
//  AddStoryVC.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 15/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire
import Photos

class AddStoryVC: UIViewController {
    
    @IBOutlet weak var txtProductName: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var lblImageName: UILabel!
    @IBOutlet weak var imgStory: UIImageView!

    var mediaPickerhelper : MediaPickerHelper?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnChooseFile(_ sender: UIButton) {
        mediaPickerhelper = MediaPickerHelper(viewController: self, isForVideo : false, imageCallback: {
            image in
            self.imgStory.image = (image?.resized(withPercentage: 0.3))!
            //            self.strImageFile = "1"
            
        }, videoCallback: nil)
        
        mediaPickerhelper?.titleForActionSheet = "Select option for upload image"
        mediaPickerhelper?.titleForCameraButton = "Camera"
        mediaPickerhelper?.titleForPhotosButton = "Photos"
        mediaPickerhelper?.showPhotoSourceSelection()
    }
    @IBAction func btnUpload(_ sender: UIButton) {
        AddStoryWebservice()
    }
    
    
    //MARK:- API WEBSERVICES
    func AddStoryWebservice(){
        self.view.endEditing(false)
        
        let urlString = createUrlForWebservice(url: APIManager.ADD_STORY_URL)
        
        let strName = txtProductName.text ?? ""
        let strPrice = txtPrice.text ?? ""
        
        var stUserID = 0
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }

        let param:Parameters = ["user_id":"\(stUserID)","name":strName,"price":strPrice]
        
        print(param)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithMultipartDataURL(urlString, method: .post, imgProfile: imgStory.image, imgName: "image", queryParameters: nil, bodyParameters: param, headers: headers, completionHandler: { (response) in
            
            indicator.shared.hideIndicator()
            var responseData:CommonModel?
            responseData = CommonModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.navigationController?.popViewController(animated: true)
            }else{
                AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: responseData?.message ?? "Something wrong.", viewcontroller: self)
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
            AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: AlertMessages.kmsgServerError, viewcontroller: self)
        }
    }
}
