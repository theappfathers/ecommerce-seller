//
//  ProductDetailsVC.swift
//  eCommerce Buyer
//
//  Created by Theappfathers on 08/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire

class ProductDetailsVC: UIViewController {
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var imgProducts: UIImageView!
    @IBOutlet weak var collAllSizes: UICollectionView!
    @IBOutlet weak var collAllColors: UICollectionView!

    @IBOutlet weak var lblCartValue: UILabel!

    var arrSize = [CommonCategoryDataList]()
    var arrColors = [CommonCategoryDataList]()
    var objProductDetails:TodayTrendProductModel?
    var objProductAllDetails:AllProductDataModel?

    var isFromview = ""
    
    var IntSize = 0
    var IntColor = 0
    var IntCartValue = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
              
        if isFromview == "AllProducts"{
            setAllProductDetails()
        }else{
            setProductDetails()
        }
    }
    
    func setCartValue(value:Int){
        
        lblCartValue.text = "\(value)"
    }
    func setAllProductDetails(){
        
        lblPrice.text = "$\(objProductAllDetails?.price ?? 0)"
        lblDescription.text = objProductAllDetails?.description
        lblTitle.text = objProductAllDetails?.title
        
        let strCompany = objProductAllDetails?.image ?? ""
        imgProduct.sd_setImage(with: URL.init(string: strCompany), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
        })
        imgProducts.sd_setImage(with: URL.init(string: strCompany), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
        })
        
        collAllSizes.register(UINib.init(nibName: ProductSizeCell.userIdentifier, bundle: nil), forCellWithReuseIdentifier: ProductSizeCell.userIdentifier)
        
        collAllColors.register(UINib.init(nibName: ColorCell.userIdentifier, bundle: nil), forCellWithReuseIdentifier: ColorCell.userIdentifier)

        arrSize = objProductAllDetails?.size ?? []
        arrColors = objProductAllDetails?.color ?? []
        collAllSizes.reloadData()
        collAllColors.reloadData()

    }
    func setProductDetails(){
        
        lblPrice.text = "$\(objProductDetails?.price ?? 0)"
        lblDescription.text = objProductDetails?.description
        lblTitle.text = objProductDetails?.title
        
        let strCompany = objProductDetails?.image ?? ""
        imgProduct.sd_setImage(with: URL.init(string: strCompany), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
        })
        imgProducts.sd_setImage(with: URL.init(string: strCompany), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
        })
        
        collAllSizes.register(UINib.init(nibName: ProductSizeCell.userIdentifier, bundle: nil), forCellWithReuseIdentifier: ProductSizeCell.userIdentifier)
        
        collAllColors.register(UINib.init(nibName: ColorCell.userIdentifier, bundle: nil), forCellWithReuseIdentifier: ColorCell.userIdentifier)

        arrSize = objProductDetails?.size ?? []
        arrColors = objProductDetails?.color ?? []
        collAllSizes.reloadData()
        collAllColors.reloadData()

    }
    override func viewWillAppear(_ animated: Bool) {
        if let flowLayout = self.collAllSizes.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 5
            flowLayout.minimumInteritemSpacing = 5
            self.collAllSizes.reloadData()
        }
        self.collAllSizes.reloadData()
        
        if let flowLayout = self.collAllColors.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 5
            flowLayout.minimumInteritemSpacing = 5
            self.collAllColors.reloadData()
        }
        self.collAllColors.reloadData()
    }
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAddToCart(_ sender: UIButton) {

    }
    @IBAction func btnFavourite(_ sender: UIButton) {

    }
    @IBAction func btnPlus(_ sender: UIButton) {
        IntCartValue = IntCartValue + 1
        setCartValue(value: IntCartValue)
    }
    @IBAction func btnMinus(_ sender: UIButton) {
        if IntCartValue > 1{
            IntCartValue = IntCartValue - 1
            setCartValue(value: IntCartValue)
        }
    }
}

extension ProductDetailsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collAllSizes{
            return arrSize.count
        }else{
            return arrColors.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collAllSizes{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductSizeCell.userIdentifier, for: indexPath) as! ProductSizeCell
            
            let objData = arrSize[indexPath.row]
            
            if objData.id == IntSize{
                cell.vwBG.backgroundColor = THEME.ThemeSelColor
                cell.vwBG.layer.borderWidth = 0.5
            }else{
                cell.vwBG.backgroundColor = THEME.ThemeBGColor
                cell.vwBG.layer.borderWidth = 0
            }
            
            cell.lblTitle.text = objData.name
            cell.setCircularImageView()
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ColorCell.userIdentifier, for: indexPath) as! ColorCell
            
            let objData = arrColors[indexPath.row]
            
            cell.vwBG.backgroundColor = hexStringToUIColor(hex: objData.name ?? "")

            if objData.id == IntColor{
                cell.imgCheck.isHidden = false
            }else{
                cell.imgCheck.isHidden = true
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collAllSizes{
            return CGSize(width: 60, height: 30)
        }else{
            return CGSize(width: 80, height: 80)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if collectionView == collAllSizes{
//            IntSize = arrSize[indexPath.row].id!
//            collAllSizes.reloadData()
//        }else{
//            IntColor = arrColors[indexPath.row].id!
//            collAllColors.reloadData()
//        }
    }
}
