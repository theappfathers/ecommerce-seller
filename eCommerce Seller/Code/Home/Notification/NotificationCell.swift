//
//  NotificationCell.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 12/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblPhotoTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescrition: UILabel!

  
     override func awakeFromNib() {
           super.awakeFromNib()
           self.imgPhoto.layer.masksToBounds = true
       }
       override var bounds: CGRect {
           didSet {
               self.layoutIfNeeded()
           }
       }
       
       override func layoutSubviews() {
           super.layoutSubviews()
           DispatchQueue.main.async {
               self.setCircularImageView()
           }
       }

       func setCircularImageView() {
           self.imgPhoto.layer.cornerRadius = 20//CGFloat(roundf(Float(self.imgPhoto.frame.height / 2.0)))
       }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
