//
//  NotificationVC.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 12/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire

class NotificationVC: UIViewController {

    @IBOutlet weak var tblNotification: UITableView!

    var arrNotification = [NotificationDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationWebservice()
    }
    //MARK:- API WEBSERVICES
    func NotificationWebservice(){
        self.view.endEditing(false)

        let urlString = createUrlForWebservice(url: APIManager.NOTIFICATION_URL)

        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .get, queryParameters: nil, bodyParameters: nil, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:Notification_ResponseModel?
            responseData = Notification_ResponseModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.arrNotification = responseData?.data ?? []
                self.tblNotification.reloadData()
            }else{
                self.tblNotification.setEmptyMessage("No Notifications")
                AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: responseData?.message ?? "Something wrong.", viewcontroller: self)
                indicator.shared.hideIndicator()
            }
            
        }) { (error) in
            indicator.shared.hideIndicator()
            AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: AlertMessages.kmsgServerError, viewcontroller: self)
        }
    }

}
extension NotificationVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotification.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        
        let objData = arrNotification[indexPath.row]
        cell.lblTitle.text = objData.name
        cell.lblDescrition.text = objData.description
        
        cell.selectionStyle = .none
        return cell
    }
}
