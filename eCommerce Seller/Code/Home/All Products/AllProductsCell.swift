//
//  AllProductsCell.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 12/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit

class AllProductsCell: UICollectionViewCell {
    static var userIdentifier = "AllProductsCell"

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
//    var activityView = UIActivityIndicatorView(style: .large)

    func showActivityIndicatory() {
//        activityView.center = imgProduct.center
//        activityView.backgroundColor = .red
//        imgProduct.addSubview(activityView)
//        activityView.startAnimating()
    }
    func stopActivityIndicatory() {
//        activityView.removeFromSuperview()
//        activityView.stopAnimating()
    }

    
}
