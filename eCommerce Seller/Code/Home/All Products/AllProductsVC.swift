//
//  AllProductsVC.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 12/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire

class AllProductsVC: UIViewController {
    
    @IBOutlet weak var collProducts: UICollectionView!
    
    var arrAllProducts = [AllProductDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collProducts.dataSource = self
        self.collProducts.delegate = self
        self.collProducts.reloadData()
//        AllProductsWebservice()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let flowLayout = self.collProducts.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            flowLayout.scrollDirection = .vertical
            flowLayout.minimumLineSpacing = 0
            flowLayout.minimumInteritemSpacing = 0
            self.collProducts.reloadData()
        }
        self.collProducts.reloadData()
        AllProductsWebservice()
    }
    
    //MARK:- API WEBSERVICES
    func AllProductsWebservice(){
        self.view.endEditing(false)
        var stUserID = 0
        
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }
        let urlString = createUrlForWebservice(url: APIManager.GET_PRODUCTLIST_URL) + "/\(stUserID)"
        
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .get, queryParameters: nil, bodyParameters: nil, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:AllProduct_ResponseModel?
            responseData = AllProduct_ResponseModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.arrAllProducts = responseData?.data ?? []
                self.collProducts.restore()
                self.collProducts.reloadData()
            }else{
                self.arrAllProducts = []
                self.collProducts.setEmptyMessage("No Products")
                indicator.shared.hideIndicator()
                self.collProducts.reloadData()

            }
        }) { (error) in
            indicator.shared.hideIndicator()
        }
    }

    func DeleteProductsWebservice(productID:String){
        self.view.endEditing(false)
        
        let urlString = createUrlForWebservice(url: APIManager.REMOVE_PRODUCT_URL)
        let param:Parameters = ["product_id":productID]
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .post, queryParameters: nil, bodyParameters: param, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:CommonCategoryDataModel?
            responseData = CommonCategoryDataModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.AskConfirmation(title: AlertTitle.Information, message: responseData?.message ?? "") { result in
                    if result{
                        self.AllProductsWebservice()
                    }
                }
            }else{
                indicator.shared.hideIndicator()
                AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: responseData?.message ?? "Something wrong.", viewcontroller: self)
            }
        }) { (error) in
            indicator.shared.hideIndicator()
            AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: AlertMessages.kmsgServerError, viewcontroller: self)
        }
    }
}
extension AllProductsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAllProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AllProductsCell.userIdentifier, for: indexPath) as! AllProductsCell
        
        let objData = arrAllProducts[indexPath.row]
        
        cell.lblTitle.text = objData.name
        cell.lblAmount.text = "$\(objData.price ?? 0)"
        cell.lblDescription.text = objData.description
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(DeleteProducts(sender:)), for: .touchUpInside)
        
        let strImgUrl = objData.image ?? ""

        cell.imgProduct.setShowActivityIndicator(true)
        cell.imgProduct.setIndicatorStyle(.gray)
        cell.imgProduct.sd_setImage(with: URL.init(string: strImgUrl), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
        })

        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.collProducts.frame.size.width) / 2.1
        let height = width * 1.5 //ratio
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextVC = STORYBOARD.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        nextVC.isFromview = "AllProducts"
        nextVC.objProductAllDetails = arrAllProducts[indexPath.row]
        self.navigationController?.pushViewController(nextVC, animated: true)

    }
    
    
    @objc func DeleteProducts(sender:UIButton){
        
        let objProductID = arrAllProducts[sender.tag].id
        
        self.AskConfirmationWithCancel(title: AlertTitle.Confirmation, message:"Are you sure want to delete this product?") { result in
            if result{
                self.DeleteProductsWebservice(productID: "\(objProductID ?? 0)")
            }
        }
    }
}

