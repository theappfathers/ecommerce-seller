//
//  OrdersCell.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 13/05/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit

class OrdersCell: UITableViewCell {
    static var userIdentifier = "OrdersCell"

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnStatus: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
