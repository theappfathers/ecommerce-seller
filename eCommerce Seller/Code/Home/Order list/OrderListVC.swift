//
//  OrderListVC.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 13/05/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire

class OrderListVC: UIViewController {

    @IBOutlet weak var tblOrders: UITableView!

    var arrAllOrders = [OrderListDataMode]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        AllOrdersWebservice()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:- API WEBSERVICES
    func AllOrdersWebservice(){
        self.view.endEditing(false)
        var stUserID = 0
        
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }
        let urlString = createUrlForWebservice(url: APIManager.GET_ORDERS_URL) + "/\(stUserID)"
        
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .get, queryParameters: nil, bodyParameters: nil, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:OrderListMode?
            responseData = OrderListMode(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.arrAllOrders = responseData?.data ?? []
                self.tblOrders.restore()
                if self.arrAllOrders.count == 0{
                    self.tblOrders.setEmptyMessage("No Orders")
                }else{
                    self.tblOrders.reloadData()
                }
            }else{
                self.arrAllOrders = []
                self.tblOrders.setEmptyMessage("No Orders")
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
        }
    }
    
    func ChangeOrderStatusWebservice(strID:String){
        self.view.endEditing(false)
        var stUserID = 0
        
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }
        let urlString = createUrlForWebservice(url: APIManager.ORDERS_STATUS_URL)
        let param:Parameters = ["status":"\(strID)","userid":"\(stUserID)"]
        
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .post, queryParameters: nil, bodyParameters: param, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:CommonModel?
            responseData = CommonModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                
                self.AllOrdersWebservice()
                
            }else{
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
        }
    }
}

extension OrderListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAllOrders.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OrdersCell.userIdentifier, for: indexPath) as! OrdersCell
        let objData = arrAllOrders[indexPath.row]
        
        cell.lblTitle.text = objData.name
//        cell.lblAmount.text = "$\(objData.price ?? "")"

        let strImgUrl = ""//objData.image ?? ""

        cell.imgProduct.setShowActivityIndicator(true)
        cell.imgProduct.setIndicatorStyle(.gray)
        cell.imgProduct.sd_setImage(with: URL.init(string: strImgUrl), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
        })


        cell.btnStatus.tag = indexPath.row
        cell.btnStatus.addTarget(self, action: #selector(ChangeStatus(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func ChangeStatus(sender:UIButton){
        let objData = arrAllOrders[sender.tag]
        ChangeOrderStatusWebservice(strID: "\(objData.id ?? 0)")
    }
}
