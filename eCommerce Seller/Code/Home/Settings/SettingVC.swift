//
//  SettingVC.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 24/05/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import ADCountryPicker

class SettingVC: UIViewController {

    @IBOutlet weak var lblCountryName: UILabel!

    var picker = ADCountryPicker(style: .grouped)

    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnProfile(_ sender: UIButton) {
        let next = STORYBOARD.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(next, animated: true)

    }
    @IBAction func btnShippingAddress(_ sender: UIButton) {
        let next = STORYBOARD.instantiateViewController(withIdentifier: "ShippingAddressVC") as! ShippingAddressVC
        self.navigationController?.pushViewController(next, animated: true)

    }
    @IBAction func btnPaymentMethods(_ sender: UIButton) {
        let next = STORYBOARD.instantiateViewController(withIdentifier: "OrderListVC") as! OrderListVC
        self.navigationController?.pushViewController(next, animated: true)

    }
    
    @IBAction func btnCountry(_ sender: UIButton) {
        self.present(picker, animated: true, completion: nil)
    }
    @IBAction func btnCurrency(_ sender: UIButton) {

    }
    @IBAction func btnSizes(_ sender: UIButton) {

    }
    @IBAction func btnTermsAndCondition(_ sender: UIButton) {

    }

    @IBAction func btnLanguages(_ sender: UIButton) {

    }
    @IBAction func btnAboutUs(_ sender: UIButton) {

    }
    @IBAction func btnLogout(_ sender: UIButton) {
        let next = STORYBOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        resetDefaults()
        self.navigationController?.pushViewController(next, animated: true)
    }
    @IBAction func btnDeleteAccount(_ sender: UIButton) {

    }
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
}

extension SettingVC : ADCountryPickerDelegate{
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        print(name)
        self.lblCountryName.text = name
        picker.dismiss(animated: true, completion: nil)
    }
}
