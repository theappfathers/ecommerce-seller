//
//  ShippingAddressVC.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 24/05/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire
import ADCountryPicker

class ShippingAddressVC: UIViewController {

    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtTownCity: UITextField!
    @IBOutlet weak var txtPostcode: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var lblCountry: UILabel!
    var picker = ADCountryPicker(style: .plain)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self

        GetShipingAddressWebservice()
    }

    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSelectCountry(_ sender: UIButton) {
        self.present(picker, animated: true, completion: nil)
    }
    @IBAction func btnSaveChanges(_ sender: UIButton) {
        if isValidation(){
            SaveShipingAddressWebservice()
        }
    }
    
    func setDetails(objData:[ShippingAddressDataModel]){
        if let objInfo = objData.first{
            lblCountry.text = objInfo.country
            txtAddress.text = objInfo.address
            txtPostcode.text = objInfo.postcode
            txtTownCity.text = objInfo.city
            txtPhoneNumber.text = objInfo.phone_number
        }
    }
    
    func GetShipingAddressWebservice(){
        self.view.endEditing(false)
        var stUserID = 0
        
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }
        let urlString = createUrlForWebservice(url: APIManager.GET_SHIPPING_ADD_URL)
        
        let param:Parameters = ["user_id":"\(stUserID)"]
        
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .post, queryParameters: nil, bodyParameters: param, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:ShippingAddressModel?
            responseData = ShippingAddressModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.setDetails(objData: responseData?.data ?? [])
            }else{
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
        }
    }

    
    func SaveShipingAddressWebservice(){
        self.view.endEditing(false)
        var stUserID = 0
        
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }
        let urlString = createUrlForWebservice(url: APIManager.SAVE_SHIPPING_ADDRESS_URL)
        
        let strAddress = txtAddress.text ?? ""
        let strTownCity = txtTownCity.text ?? ""
        let strPostcode = txtPostcode.text ?? ""
        let strPhoneNumber = txtPhoneNumber.text ?? ""
        let strCountry = lblCountry.text ?? ""
        
        let param:Parameters = ["user_id":"\(stUserID)","address":strAddress,"city":strTownCity,"postcode":strPostcode,"phone_number":strPhoneNumber,"country":strCountry]
        
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .post, queryParameters: nil, bodyParameters: param, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:CommonModel?
            responseData = CommonModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: responseData?.message ?? "", viewcontroller: self)

            }else{
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
        }
    }
}

extension ShippingAddressVC{
    func isValidation() -> Bool{
       if (txtAddress.text?.isEmpty)!{
           AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankAddress, viewcontroller: self)

           return false
       }
        if (txtTownCity.text?.isEmpty)!{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlanTown, viewcontroller: self)

            return false
        }
        if (txtPostcode.text?.isEmpty)! {
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankPostcode, viewcontroller: self)
            return false
        }
        if (txtPhoneNumber.text?.isEmpty)! {
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlanPhoneNumber, viewcontroller: self)
            return false
        }
        return true
    }
}
extension ShippingAddressVC : ADCountryPickerDelegate{
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        print(name)
        self.lblCountry.text = name
        picker.dismiss(animated: true, completion: nil)
    }
}
