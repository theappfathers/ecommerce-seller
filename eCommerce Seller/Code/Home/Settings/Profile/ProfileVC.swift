//
//  ProfileVC.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 24/05/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire

class ProfileVC: UIViewController {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var mediaPickerhelper : MediaPickerHelper?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCircularImageView()
        GetProfileWebservice()
        // Do any additional setup after loading the view.
    }
    func setCircularImageView() {
        self.imgProfile.layer.cornerRadius = CGFloat(roundf(Float(self.imgProfile.frame.height / 2.0)))
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveChanges(_ sender: UIButton) {
        if isValidation(){
            SaveProfileWebservice()
        }
    }
    
    @IBAction func btnEditDetails(_ sender: UIButton) {
        mediaPickerhelper = MediaPickerHelper(viewController: self, isForVideo : false, imageCallback: {
            image in
//            self.imgProfile.image = (image?.resized(withPercentage: 0.3))!
            let size = CGSize(width: 150, height: 150)
            let imagea = image?.crop(to: size)
            self.imgProfile.image = imagea

            
        }, videoCallback: nil)
        
        mediaPickerhelper?.titleForActionSheet = "Select option for upload image"
        mediaPickerhelper?.titleForCameraButton = "Camera"
        mediaPickerhelper?.titleForPhotosButton = "Photos"
        mediaPickerhelper?.showPhotoSourceSelection()
    }
    
    func setDetails(objData:ProfileDataModel){
        txtEmail.text = objData.email
        txtUsername.text = objData.company_name
        
        let strImgUrl = objData.image ?? ""
        imgProfile.setShowActivityIndicator(true)
        imgProfile.setIndicatorStyle(.gray)
        imgProfile.sd_setImage(with: URL.init(string: strImgUrl), placeholderImage: UIImage(named: "App-Default"), completed: { (image, error, cacheType, imageURL) in
        })
    }
    
    func GetProfileWebservice(){
        self.view.endEditing(false)
        var stUserID = 0
        
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }
        let urlString = createUrlForWebservice(url: APIManager.GET_PROFILE_URL)
                
        let param:Parameters = ["user_id":"\(stUserID)"]
        
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .post, queryParameters: nil, bodyParameters: param, headers: headers, encodeType: URLEncoding(), completionHandler: { response in

            indicator.shared.hideIndicator()
            var responseData:ProfileModel?
            responseData = ProfileModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.setDetails(objData: responseData!.data!)
            }else{
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
        }
    }
    func SaveProfileWebservice(){
        self.view.endEditing(false)
        var stUserID = 0
        
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }
        let urlString = createUrlForWebservice(url: APIManager.SAVE_PROFILE_URL)
        
        let strUsername = txtUsername.text ?? ""
        let strEmail = txtEmail.text ?? ""
        let strPassword = txtPassword.text ?? ""
        
        let param:Parameters = ["user_id":"\(stUserID)","company_name":strUsername,"email":strEmail,"password":strPassword,"c_password":strPassword]
        
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithMultipartDataURL(urlString, method: .post, imgProfile: imgProfile.image, imgName: "image", queryParameters: nil, bodyParameters: param, headers: headers, completionHandler: { (response) in

            indicator.shared.hideIndicator()
            var responseData:CommonModel?
            responseData = CommonModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: responseData?.message ?? "", viewcontroller: self)
                
            }else{
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
        }
    }
}
extension ProfileVC{
    func isValidation() -> Bool{
       if (txtUsername.text?.isEmpty)!{
           AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankUsername, viewcontroller: self)

           return false
       }
        if (txtEmail.text?.isEmpty)!{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankEmail, viewcontroller: self)

            return false
        }
        if !isValidEmail(testStr: txtEmail.text!){
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgValidEmail, viewcontroller: self)

            return false
        }
//        if (txtPassword.text?.isEmpty)! {
//            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankPassword, viewcontroller: self)
//            return false
//        }
        return true
    }
}
