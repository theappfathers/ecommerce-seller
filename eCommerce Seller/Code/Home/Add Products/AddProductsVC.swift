//
//  AddProductsVC.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 12/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import Alamofire

class AddProductsVC: UIViewController {
    
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnSubCategory: UIButton!
    
    @IBOutlet weak var txtProductName: UITextField!
    @IBOutlet weak var txtProductTitle: UITextField!
    @IBOutlet weak var txtProductCode: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var txtProductDescription: UITextView!
    @IBOutlet weak var collSize: UICollectionView!
    @IBOutlet weak var collAllColors: UICollectionView!
    @IBOutlet weak var imgProduct: UIImageView!
    
    
    var mediaPickerhelper : MediaPickerHelper?
    
    var arrCategory = [CategoryDataList]()
    
    var arrColor = [CommonCategoryDataList]()
    var arrSize = [CommonCategoryDataList]()
    var arrType = [CommonCategoryDataList]()

    var subCategoryDropdown = DropDown()
    var categoryDropdown = DropDown()
    var typeDropdown = DropDown()

    var imgProducts = UIImage()
    var isImage = false
    var IntSubCatID = -1
    var IntCatID = -1
    var intType = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collSize.register(UINib.init(nibName: ProductSizeCell.userIdentifier, bundle: nil), forCellWithReuseIdentifier: ProductSizeCell.userIdentifier)
        
        collAllColors.register(UINib.init(nibName: ColorCell.userIdentifier, bundle: nil), forCellWithReuseIdentifier: ColorCell.userIdentifier)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        clearProductData()
        CategoryWebservice()
        if let flowLayout = self.collSize.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 5
            flowLayout.minimumInteritemSpacing = 5
            self.collSize.reloadData()
        }
        self.collSize.reloadData()
        
        if let flowLayout = self.collAllColors.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 5
            flowLayout.minimumInteritemSpacing = 5
            self.collAllColors.reloadData()
        }
        self.collAllColors.reloadData()
        
    }
    @IBAction func btnType(_ sender: UIButton) {
        typeDropdown.show()
    }
    @IBAction func btnCategory(_ sender: UIButton) {
        categoryDropdown.show()
    }
    @IBAction func btnSubCategory(_ sender: UIButton) {
        subCategoryDropdown.show()
    }
    
    @IBAction func btnAddProduct(_ sender: UIButton) {
        if isValidation(){
            AddProductsWebservice()
        }
    }
    
    @IBAction func btnChooseFile(_ sender: UIButton) {
        
        mediaPickerhelper = MediaPickerHelper(viewController: self, isForVideo : false, imageCallback: {
            image in
            self.imgProducts = (image?.resized(withPercentage: 0.3))!
            self.imgProduct.image = (image?.resized(withPercentage: 0.3))!
            self.lblFileName.text = "image.png"
            self.isImage = true
        }, videoCallback: nil)
        
        mediaPickerhelper?.titleForActionSheet = "Select option for upload image"
        mediaPickerhelper?.titleForCameraButton = "Camera"
        mediaPickerhelper?.titleForPhotosButton = "Photos"
        mediaPickerhelper?.showPhotoSourceSelection()        
    }
    
    func GetCategory(){
        let arrFilter = arrCategory.map{$0.name}
        
        categoryDropdown.anchorView = btnCategory
        categoryDropdown.bottomOffset = CGPoint(x: 0, y:(categoryDropdown.anchorView?.plainView.bounds.height)!)
        categoryDropdown.dataSource = arrFilter as! [String]
        categoryDropdown.backgroundColor = UIColor.white
        categoryDropdown.shadwColor = UIColor.lightGray
        categoryDropdown.shadwRadius = 1.5
        categoryDropdown.shadwOffset = CGSize(width: 0.0, height: 0.0)
        categoryDropdown.shadwOpacity = 0.5
        categoryDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.btnCategory.setTitle(item, for: .normal)
            self.IntCatID = self.arrCategory[index].id ?? 0
            self.GetSubCategory(arrSubCategory: self.arrCategory[index].sub_category)
        }
    }
    func GetTypeList(){
        
        let arrFilterType = arrType.map{$0.name}
        
        typeDropdown.anchorView = btnType
        typeDropdown.bottomOffset = CGPoint(x: 0, y:(typeDropdown.anchorView?.plainView.bounds.height)!)
        typeDropdown.dataSource = arrFilterType as! [String]
        typeDropdown.backgroundColor = UIColor.white
        typeDropdown.shadwColor = UIColor.lightGray
        typeDropdown.shadwRadius = 1.5
        typeDropdown.shadwOffset = CGSize(width: 0.0, height: 0.0)
        typeDropdown.shadwOpacity = 0.5
        typeDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.btnType.setTitle(item, for: .normal)
            self.intType = self.arrType[index].id ?? 0
            self.getCategoryWebservice(strTypeID: "\(self.intType)")
        }
        
    }
    func GetSubCategory(arrSubCategory:[SubCategoryDataList]){
        let arrFilter = arrSubCategory.map{$0.name}
        
        subCategoryDropdown.anchorView = btnSubCategory
        subCategoryDropdown.bottomOffset = CGPoint(x: 0, y:(subCategoryDropdown.anchorView?.plainView.bounds.height)!)
        subCategoryDropdown.dataSource = arrFilter as! [String]
        subCategoryDropdown.backgroundColor = UIColor.white
        subCategoryDropdown.shadwColor = UIColor.lightGray
        subCategoryDropdown.shadwRadius = 1.5
        subCategoryDropdown.shadwOffset = CGSize(width: 0.0, height: 0.0)
        subCategoryDropdown.shadwOpacity = 0.5
        subCategoryDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.btnSubCategory.setTitle(item, for: .normal)
            self.IntSubCatID = arrSubCategory[index].id ?? 0
        }
    }
    
    func clearProductData(){
        txtProductName.text = ""
        txtProductTitle.text = ""
        txtProductCode.text = ""
        txtProductDescription.text = "Product Description"
        txtPrice.text = ""
        txtQuantity.text = ""
        imgProducts = UIImage()
        imgProduct.image = UIImage()
        lblFileName.text = "No File Choosen"
        isImage = false
        arrSize.forEach { $0.isSelected = false }
        arrColor.forEach { $0.isSelected = false }
        btnSubCategory.setTitle("Select Sub Category", for: .normal)
        btnCategory.setTitle("Select Category", for: .normal)

        intType = -1
        IntCatID = -1
        IntSubCatID = -1
        
        collSize.reloadData()
        collAllColors.reloadData()
    }
    
    //MARK:- API WEBSERVICES
    
    func AddProductsWebservice(){
        self.view.endEditing(false)
        
        let urlString = createUrlForWebservice(url: APIManager.ADD_PRODUCT_URL)
        
        let strProductName = txtProductName.text ?? ""
        let strProductTitle = txtProductTitle.text ?? ""
        let strProductCode = txtProductCode.text ?? ""
        let strDesc = txtProductDescription.text ?? ""
        let strPrice = txtPrice.text ?? ""
        let strQuantity = txtQuantity.text ?? "1"
        
        var stUserID = 0
        
        if let user = AppData.shared.getUserInfo() {
            stUserID = user.id ?? 0
        }
        let arrSizes = arrSize.filter { $0.isSelected == true }.compactMap { $0.id }
        let formattedSize = (arrSizes.map{ "\($0)" }).joined(separator: ",")
        
        let arrColors = arrColor.filter { $0.isSelected == true }.compactMap { $0.id }
        let formattedColor = (arrColors.map{ "\($0)" }).joined(separator: ",")
        
        
        let param:Parameters = ["user_id":"\(stUserID)","name":strProductName,"title":strProductTitle,"code":strProductCode,"description":strDesc,"price":strPrice,"qty":strQuantity,"style":"fasion","type_id":"\(intType)","size_id":formattedSize,"color_id":formattedColor,"category_id":"\(IntCatID)","sub_category_id":"\(IntSubCatID)"]
        
        print(param)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithMultipartDataURL(urlString, method: .post, imgProfile: imgProducts, imgName: "image", queryParameters: nil, bodyParameters: param, headers: headers, completionHandler: { (response) in
            
            indicator.shared.hideIndicator()
            var responseData:CommonModel?
            responseData = CommonModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.clearProductData()
                AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: responseData?.message ?? "", viewcontroller: self)
            }else{
                AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: responseData?.message ?? "Something wrong.", viewcontroller: self)
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
            AlertView.showOKTitleMessageAlert(AlertTitle.Information, strMessage: AlertMessages.kmsgServerError, viewcontroller: self)
        }
    }
    
    func getCategoryWebservice(strTypeID:String){
        self.view.endEditing(false)
        
        let urlString = createUrlForWebservice(url: APIManager.GET_TYPE_CATEGORY) + "/\(strTypeID)"
        
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .get, queryParameters: nil, bodyParameters: nil, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:CategoryDataModel?
            responseData = CategoryDataModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                self.arrCategory = responseData?.data ?? []
                self.GetCategory()
            }else{
                self.arrCategory = []
                
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
        }
    }
    
    
    func CategoryWebservice(){
        self.view.endEditing(false)
        
        let urlString = createUrlForWebservice(url: APIManager.EXTRA_URL)
        
        print(urlString)
        indicator.shared.showIndicator()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        WebServiceManager.WebService.sendRequestWithURL(urlString, method: .get, queryParameters: nil, bodyParameters: nil, headers: headers, encodeType: URLEncoding(), completionHandler: { response in
            
            indicator.shared.hideIndicator()
            var responseData:CommonCategoryDataModel?
            responseData = CommonCategoryDataModel(JSON: response as! [String : Any])
            
            if responseData?.code == 200{
                
//                self.arrCategory = responseData?.data?.category ?? []
                self.arrColor = responseData?.data?.color ?? []
                self.arrSize = responseData?.data?.size ?? []
                self.arrType = responseData?.data?.type ?? []
                
                self.collSize.reloadData()
                self.collAllColors.reloadData()
                self.GetTypeList()
                
            }else{
                indicator.shared.hideIndicator()
            }
        }) { (error) in
            indicator.shared.hideIndicator()
        }
    }
}


extension AddProductsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collSize{
            return arrSize.count
        }else{
            return arrColor.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collSize{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductSizeCell.userIdentifier, for: indexPath) as! ProductSizeCell
            
            let objData = arrSize[indexPath.row]
            
            if objData.isSelected == true{
                cell.vwBG.backgroundColor = THEME.ThemeSelColor
            }else{
                cell.vwBG.backgroundColor = THEME.ThemeBGColor
            }
            
            cell.lblTitle.text = objData.name
            cell.setCircularImageView()
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ColorCell.userIdentifier, for: indexPath) as! ColorCell
            
            let objData = arrColor[indexPath.row]
            
            cell.vwBG.backgroundColor = UIColor.init(hexString: objData.name ?? "")
            
            if objData.isSelected == true{
                cell.imgCheck.isHidden = false
            }else{
                cell.imgCheck.isHidden = true
            }
            
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collSize{
            return CGSize(width: 60, height: 30)
        }else{
            return CGSize(width: 80, height: 80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collSize{
            if arrSize[indexPath.row].isSelected {
                arrSize[indexPath.row].isSelected = false
            }else{
                arrSize[indexPath.row].isSelected = true
            }
            collSize.reloadData()
        }else{
            if arrColor[indexPath.row].isSelected {
                arrColor[indexPath.row].isSelected = false
            }else{
                arrColor[indexPath.row].isSelected = true
            }
            
            collAllColors.reloadData()
        }
    }
}

extension AddProductsVC : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Product Description"{
            txtProductDescription.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            txtProductDescription.text = "Product Description"
        }
    }
}

extension AddProductsVC {
    func isValidation() -> Bool{
        if (txtProductName.text?.isEmpty)!{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductName, viewcontroller: self)
            
            return false
        }
        if (txtProductTitle.text?.isEmpty)!{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductTitle, viewcontroller: self)
            
            return false
        }
        if (txtProductCode.text?.isEmpty)! {
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductCode, viewcontroller: self)
            return false
        }
        if isImage == false{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductImage, viewcontroller: self)
            return false
        }
        
        if (txtPrice.text?.isEmpty)! {
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductPrice, viewcontroller: self)
            return false
        }
        var isColor = false
        arrColor.forEach {
            if $0.isSelected == true{
                isColor = true
            }
        }
        if isColor == false{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductColor, viewcontroller: self)
            return false
        }
        
        if intType == -1{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductType, viewcontroller: self)
            return false
        }
        
        if txtProductDescription.text == "Product Description" {
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductDesc, viewcontroller: self)
            return false
        }
        
        if IntCatID == -1{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductCate, viewcontroller: self)
            return false
        }
        
        var isSize = false
        arrSize.forEach {
            if $0.isSelected == true{
                isSize = true
            }
        }
        if isSize == false{
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductSize, viewcontroller: self)
            return false
        }
        
        if IntSubCatID == -1 {
            AlertView.showOKTitleMessageAlert(AlertTitle.App_Name, strMessage: AlertMessages.kmsgBlankProductSubCate, viewcontroller: self)
            return false
        }
        
        
        return true
    }
    
}
