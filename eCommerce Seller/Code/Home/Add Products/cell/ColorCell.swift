//
//  ColorCell.swift
//  eCommerce Buyer
//
//  Created by Theappfathers on 29/05/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit

class ColorCell: UICollectionViewCell {
    static var userIdentifier = "ColorCell"
    
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var imgCheck: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.vwBG.layer.masksToBounds = true
    }
    override var bounds: CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.setCircularImageView()
        }
    }
    
    func setCircularImageView() {
        self.vwBG.layer.cornerRadius = 5
    }
    
}
