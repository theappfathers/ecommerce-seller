//
//  ProductSizeCell.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 30/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit

class ProductSizeCell: UICollectionViewCell {
    static var userIdentifier = "ProductSizeCell"
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwBG: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.vwBG.layer.masksToBounds = true
    }
    override var bounds: CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.setCircularImageView()
        }
    }

    func setCircularImageView() {
        self.vwBG.layer.cornerRadius = 5// CGFloat(roundf(Float(self.vwBG.frame.height / 2.0)))
    }

}
