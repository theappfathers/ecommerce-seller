//
//  Utility.swift
//
//  Created by Mac on 27/06/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SystemConfiguration
import SVProgressHUD
import ReachabilitySwift
import Alamofire
import ARSLineProgress
import DSGradientProgressView

//MARK:- Check internet connectivity
//public class Reachability {
//
//    class func isConnectedToNetwork() -> Bool {
//
//        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
//        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
//        zeroAddress.sin_family = sa_family_t(AF_INET)
//
//        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
//            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
//                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
//            }
//        }
//
//        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
//        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
//            return false
//        }
//
//        // Working for Cellular and WIFI
//        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
//        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
//        let ret = (isReachable && !needsConnection)
//
//        return ret
//
//    }
//}

func ShowProgress(){
    SVProgressHUD.show()
}

func HideProgress(){
    SVProgressHUD.dismiss()
}

//MARK:- Save, Fetch, Get Model
//==========================================

func addBoolToUserDefaults(pBool: Bool, pForKey:String){
    let defaults = UserDefaults.standard
    defaults.set(pBool, forKey: pForKey)
    defaults.synchronize()
}

func getBoolFromUserDefaults(pForKey:String)->Bool{
    var valid = Bool()
    let defaults = UserDefaults.standard
    valid = defaults.bool(forKey: pForKey)
    return valid
}

func saveStringValue(strValue : String, forKey key : String) {
    UserDefaults.standard.set(strValue, forKey: key)
    UserDefaults.standard.synchronize()
}

func getStringValue(key : String) ->String{
    return UserDefaults.standard.object(forKey: key) as? String ?? "0"
}

func saveModel(_ model: Any, forKey key: String) {
    let ud = UserDefaults.standard
    let archivedPool = try? NSKeyedArchiver.archivedData(withRootObject: model, requiringSecureCoding: true)
    ud.set(archivedPool, forKey: key)
}

func getModelForKey<T>(_ key: String) -> T? {
    let ud = UserDefaults.standard
    if let val = ud.value(forKey: key) as? Data,
        let obj = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(val) as? T {
        return obj
    }
    return nil
}

//func saveModel(_ model : AnyObject, forKey key : String) {
//    let encodedObject = NSKeyedArchiver.archivedData(withRootObject: model)
//    UserDefaults.standard.set(encodedObject, forKey: key)
//    UserDefaults.standard.synchronize()
//}
//
//func getModelForKey(_ key : String) -> AnyObject? {
//    let encodedObject = UserDefaults.standard.object(forKey: key) as? Data
//    let savedModel = encodedObject != nil ? NSKeyedUnarchiver.unarchiveObject(with: encodedObject!) : nil
//    return savedModel as AnyObject?
//}

func removeModelForKey(_ key : String) {
    UserDefaults.standard.removeObject(forKey: key)
    UserDefaults.standard.synchronize()
}

func removeUserInfo(key: String)
{
    removeModelForKey(key)
    UserDefaults.standard.removeObject(forKey: "socialLoginUserInfo")
    UserDefaults.standard.synchronize()
}


//func saveRestaurantInfo(_ restaurantInfo: RestaurantProfileDataModel) {
//    saveModel(restaurantInfo, forKey: UserDefaultsKey.restaurantData)
//}
//
//func getRestaurantInfo() -> RestaurantProfileDataModel? {
//    return getModelForKey(UserDefaultsKey.restaurantData) as? RestaurantProfileDataModel
//}
//==========================================

//MARK:- TabBar
func changeTabItemImage(tabBar: UITabBarController, index: Int, newNotifications: Bool)
{
    if newNotifications {
        tabBar.tabBar.items?[index].image = UIImage(named: "bell_selected")
        tabBar.tabBar.items?[index].selectedImage = UIImage(named: "bell_selected_blue")
    } else {
        tabBar.tabBar.items?[index].image = UIImage(named: "bell")
        tabBar.tabBar.items?[index].selectedImage = UIImage(named: "bell_blue")
    }
}

//MARK:- Thumbnail generator from video url
func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
    DispatchQueue.global().async { //1
        let asset = AVAsset(url: url) //2
        let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
        avAssetImageGenerator.appliesPreferredTrackTransform = true //4
        let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
        do {
            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
            let thumbImage = UIImage(cgImage: cgThumbImage) //7
            DispatchQueue.main.async { //8
                completion(thumbImage) //9
            }
        } catch {
            print(error.localizedDescription) //10
            DispatchQueue.main.async {
                completion(nil) //11
            }
        }
    }
}

//MARK:- UILabel
public class UIPaddedLabel: UILabel
{
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0
    
    public override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    public override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
    
    public override func sizeToFit() {
        super.sizeThatFits(intrinsicContentSize)
    }
}

//MARK:- UITextfield
func addTextFieldpadding(txtFields: [UITextField], paddingLeft : CGFloat, paddingRight : CGFloat) {
    for textfield in txtFields
    {
        let paddingLeft = UIView(frame: CGRect(x: 0, y: 0, width: paddingLeft, height: textfield.frame.size.height))
        textfield.leftView = paddingLeft
        textfield.leftViewMode = .always
        
        let paddingRight = UIView(frame: CGRect(x: 0, y: 0, width: paddingRight, height: textfield.frame.size.height))
        textfield.rightView = paddingRight
        textfield.rightViewMode = .always
    }
}

//MARK:- UITextview
func TextViewPadding(textView: UITextView){
    textView.contentInset = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 0)
}

//MARK:- UIView
func roundSpecifiedCorners(_ corners:UIRectCorner, radius: CGFloat, views: [UIView]) {
    for view in views
    {
        if #available(iOS 11.0, *) {
//            view.clipsToBounds = true
            view.layer.cornerRadius = radius
            view.layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
        } else {
            let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.topLeft, .bottomRight], cornerRadii: CGSize(width: radius, height: radius))
            
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            view.layer.mask = mask
        }
    }
}

//MARK:- RandomNumber generator
class RandomNumber: NSObject {
    class func randomNumberWithLength (_ len : Int) -> NSString {
        let letters : NSString = "123456789"
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 0..<len{
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        return randomString
    }
}


//MARK:- UISwitch
func setSwitchSize(switches: [UISwitch], transformScale: CGFloat)
{
    for swtch in switches
    {
        swtch.transform = CGAffineTransform(scaleX: transformScale, y: transformScale)
    }
}

func setSwitchUserInteraction(switches: [UISwitch], onOff: Bool)
{
    for swtch in switches
    {
        swtch.isUserInteractionEnabled = onOff
    }
}

func setSwitchOnOff(switches: [UISwitch], onOff: Bool)
{
    for swtch in switches
    {
        swtch.setOn(onOff, animated: true)
        if onOff == false {
            swtch.isEnabled = false
        } else {
            swtch.isEnabled = true
        }
    }
}

func setSwitchOnOffWithoutEnable(switches: [UISwitch], onOff: Bool)
{
    for swtch in switches
    {
        swtch.setOn(onOff, animated: true)
    }
}


//MARK:- Gradient methods
func addGradientBorder(view: UIView, gradientColors: [CGColor])
{
    let gradient = CAGradientLayer()
    gradient.frame =  CGRect(origin: CGPoint.zero, size: view.bounds.size)
    gradient.colors = gradientColors
    
    let shape = CAShapeLayer()
    shape.lineWidth = 2
    shape.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: view.layer.cornerRadius).cgPath//UIBezierPath(rect: view.bounds).cgPath
    shape.strokeColor = UIColor.black.cgColor
    shape.fillColor = UIColor.clear.cgColor
    gradient.mask = shape
    
    view.layer.addSublayer(gradient)
}

func addGradientBackground(view: UIView, gradientColors: [CGColor], direction: gradientDirection) {
    
    let gradient: CAGradientLayer = CAGradientLayer()
    gradient.colors = gradientColors
    
    if direction == .bottomTop {
        gradient.locations = [0.2, 1.0]
    } else {
        gradient.locations = [0.0 , 1.0]
    }
    
    switch direction
    {
    case .topBottom:    //left to right
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
    case .bottomTop:
        gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 0.0)
    case .leftRight:
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
    case .rightLeft:
        gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
    case .TopLeftToBottomRight:
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
    case .TopRightToBottomLeft:
        gradient.startPoint = CGPoint(x: 1.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
    case .BottomLeftToTopRight:
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
    case .BottomRightToTopLeft:
        gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
    }
//    gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
//    gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
    gradient.frame = view.layer.bounds
    view.layer.insertSublayer(gradient, at: 0)
    
}

enum gradientDirection {//darkTolight
    case topBottom
    case bottomTop
    case leftRight
    case rightLeft
    case TopLeftToBottomRight
    case TopRightToBottomLeft
    case BottomLeftToTopRight
    case BottomRightToTopLeft
}

extension CALayer {
    
    func removeAllGradiantLayers(){
        self.sublayers?.forEach
            { layer in
                if layer.isKind(of: CAGradientLayer.self)
                {
                    layer.removeFromSuperlayer()
                }
        }
    }
}

// MARK: - Toast Methods
func showToastMsg(message : String) {
    let toastLabel = UILabel(frame: CGRect(x: UIApplication.shared.keyWindow!.frame.size.width/2 - 150, y: (UIApplication.shared.keyWindow?.frame.size.height)!-120, width:300,  height : 50))
    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    toastLabel.textColor = UIColor.white
    toastLabel.textAlignment = NSTextAlignment.center;
    //appDelegate.window!.addSubview(toastLabel)
    UIApplication.shared.keyWindow?.addSubview(toastLabel)
    toastLabel.text = message
    toastLabel.numberOfLines = 0
    toastLabel.alpha = 1.0
    toastLabel.font = UIFont(name: "Montserrat-Light", size: 5.0)
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    UIView.animate(withDuration: 6, delay: 0.1, options: UIView.AnimationOptions.curveEaseOut, animations: {
        toastLabel.alpha = 0.0
    })
}

//MARK:- UIColor
func hexStringToUIColor(hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

//MARK:- Date functions
func getDayNameBy(stringDate: String) -> String
{
    let df = DateFormatter()
    df.dateFormat = "YYYY-MM-dd"
    let date = df.date(from: stringDate)!
    df.dateFormat = "EEEE"
    return df.string(from: date);
}

// MARK: - Data Conversion funcs from array/dict to json or vice versa
func ModelToDic(Model:AnyObject) -> NSDictionary{
    
    let theDict = NSMutableDictionary()
    let mirrored_object = Mirror(reflecting: Model)
    for (_, attr) in mirrored_object.children.enumerated() {
        if let name = attr.label {
            theDict.setValue( Model.value(forKey:name) , forKey: name)
        }
    }
    
    return theDict
}

func json(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options:.prettyPrinted) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}


func DictToJson(Dict:[String:Any]) -> String? {
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: Dict, options: JSONSerialization.WritingOptions.init(rawValue: 0))
        if let jsonString =  NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String? {
            return jsonString
        }
    }catch let error as NSError {
        print(error)
    }
    return nil
}

func JsonToDict(Json:String) -> [String:Any]? {
    if let data = Json.data(using: String.Encoding.utf8) {
        do{
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
        }catch let error as NSError {
            print(error)
        }
    }
    return nil
}

func JsonToArrOfDict(Json:String) -> [[String:Any]]? {
    if let data = Json.data(using: String.Encoding.utf8) {
        do{
            return try JSONSerialization.jsonObject(with: data, options: []) as? [[String:AnyObject]]
        }catch let error as NSError {
            print(error)
        }
    }
    return nil
}
public class CentersecondAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    public override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let oldAttributes = super.layoutAttributesForElements(in: rect) else {
            return nil
        }
        let attributes = oldAttributes.map { $0.copy() as! UICollectionViewLayoutAttributes }
        
        // We will do centering alignment only on the Cell layout attributes
        let cellAttributes = attributes.filter({ (layout: UICollectionViewLayoutAttributes) -> Bool in
            return layout.representedElementCategory == .cell
        })
        
        let attributesForRows: [[UICollectionViewLayoutAttributes]] = groupLayoutAttributesByRow(cellAttributes)
        
        for attributesInRow in attributesForRows {
            let sortedItem = attributesInRow.sorted(by: { $0.frame.minX < $1.frame.minX })
            
            // Layout this row layout attributes with spacing equals to layout's `minimumInteritemSpacing`.
            sortedItem.first?.frame.origin.x = 0.0
            let (neighborsElements) = zip(sortedItem.dropLast(), sortedItem.dropFirst())
            for neighborsElement in neighborsElements {
                neighborsElement.1.frame.origin.x = neighborsElement.0.frame.maxX + self.minimumInteritemSpacing
            }
            
            let (minX, maxX) = attributesInRow.reduce((CGFloat.greatestFiniteMagnitude, CGFloat.leastNormalMagnitude), { (value, attributes) -> (CGFloat, CGFloat) in
                return (min(value.0, attributes.frame.minX), max(value.1, attributes.frame.maxX))
            })
            
            let cellViewPortWidth = maxX - minX
            // Calculate the offset of this row, and apply it to this row's layout attributes to make them center aligned
            let offset = floor((self.collectionViewContentSize.width - cellViewPortWidth) / 2)
            attributesInRow.forEach({
                $0.frame.origin.x += offset
            })
        }
        
        return attributes
    }
}

/// A simple data structure to represent the row of UICollectionViewCell layout attributes.
/// Consist of top Y position and the height of this row
private struct RowAttributes {
    var y: CGFloat
    var height: CGFloat
    
    func contains(_ rect: CGRect) -> Bool {
        return rect.maxY > self.y && rect.minY < (self.y + self.height)
    }
    func attributeByUnionRect(_ rect: CGRect) -> RowAttributes {
        let y = min(self.y, rect.minY)
        let height = max(self.height + self.y, rect.maxY) - y
        return RowAttributes(y: y, height: height)
    }
}

/// Calculate the row attributes for row of the given layout attributes by walking through the array of layout attributes
private func rowAttributesOfAttributesSameRowAs(_ layoutAttributes: UICollectionViewLayoutAttributes, byAttributes attributes: [UICollectionViewLayoutAttributes]) -> RowAttributes {
    let seedRowAttributes = RowAttributes(y: layoutAttributes.frame.minY, height: layoutAttributes.frame.height)
    return  attributes.reduce(seedRowAttributes, { (attribute, layoutAttributes) -> RowAttributes in
        if attribute.contains(layoutAttributes.frame) {
            return attribute.attributeByUnionRect(layoutAttributes.frame)
        } else {
            return attribute
        }
    })
}

/// Grouping the layout attributes by their row
private func groupLayoutAttributesByRow(_ layoutAttributes: [UICollectionViewLayoutAttributes]) -> [[UICollectionViewLayoutAttributes]] {
    var groupedLayoutAttributes = [[UICollectionViewLayoutAttributes]]()
    var remainingLayoutAttributes = layoutAttributes
    while let attributes = remainingLayoutAttributes.first {
        let attributes = rowAttributesOfAttributesSameRowAs(attributes, byAttributes: remainingLayoutAttributes)
        let classifiedRects = remainingLayoutAttributes.filter{ attributes.contains($0.frame) }
        groupedLayoutAttributes.append(classifiedRects)
        
        remainingLayoutAttributes = remainingLayoutAttributes.filter{ !attributes.contains($0.frame) }
    }
    
    return groupedLayoutAttributes
}

class CenterAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let superAttributes = super.layoutAttributesForElements(in: rect) else { return nil }
        // Copy each item to prevent "UICollectionViewFlowLayout has cached frame mismatch" warning
        guard let attributes = NSArray(array: superAttributes, copyItems: true) as? [UICollectionViewLayoutAttributes] else { return nil }
        
        // Constants
        let leftPadding: CGFloat = 8
        let interItemSpacing = minimumInteritemSpacing
        
        // Tracking values
        var leftMargin: CGFloat = leftPadding // Modified to determine origin.x for each item
        var maxY: CGFloat = -1.0 // Modified to determine origin.y for each item
        var rowSizes: [[CGFloat]] = [] // Tracks the starting and ending x-values for the first and last item in the row
        var currentRow: Int = 0 // Tracks the current row
        attributes.forEach { layoutAttribute in
            
            // Each layoutAttribute represents its own item
            if layoutAttribute.frame.origin.y >= maxY {
                
                // This layoutAttribute represents the left-most item in the row
                leftMargin = leftPadding
                
                // Register its origin.x in rowSizes for use later
                if rowSizes.count == 0 {
                    // Add to first row
                    rowSizes = [[leftMargin, 0]]
                } else {
                    // Append a new row
                    rowSizes.append([leftMargin, 0])
                    currentRow += 1
                }
            }
            
            layoutAttribute.frame.origin.x = leftMargin
            
            leftMargin += layoutAttribute.frame.width + interItemSpacing
            maxY = max(layoutAttribute.frame.maxY, maxY)
            
            // Add right-most x value for last item in the row
            rowSizes[currentRow][1] = leftMargin - interItemSpacing
        }
        
        // At this point, all cells are left aligned
        // Reset tracking values and add extra left padding to center align entire row
        leftMargin = leftPadding
        maxY = -1.0
        currentRow = 0
        attributes.forEach { layoutAttribute in
            
            // Each layoutAttribute is its own item
            if layoutAttribute.frame.origin.y >= maxY {
                
                // This layoutAttribute represents the left-most item in the row
                leftMargin = leftPadding
                
                // Need to bump it up by an appended margin
                let rowWidth = rowSizes[currentRow][1] - rowSizes[currentRow][0] // last.x - first.x
                let appendedMargin = (collectionView!.frame.width - leftPadding  - rowWidth - leftPadding) / 2
                leftMargin += appendedMargin
                
                currentRow += 1
            }
            
            layoutAttribute.frame.origin.x = leftMargin
            
            leftMargin += layoutAttribute.frame.width + interItemSpacing
            maxY = max(layoutAttribute.frame.maxY, maxY)
        }
        
        return attributes
    }
}
func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}


class GLOBAL : NSObject {
    
    //sharedInstance
    static let sharedInstance = GLOBAL()
    
    
    //MARK: - Internet Reachability
    var reachability: Reachability?
    var isInternetReachable : Bool? = false
    
    func setupReachability(_ hostName: String?) {
        
        GLOBAL.sharedInstance.reachability = hostName == nil ? Reachability() : Reachability(hostname: hostName!)
        
        if reachability?.isReachable == true
        {
            GLOBAL.sharedInstance.isInternetReachable = true
        }
        
        NotificationCenter.default.addObserver(GLOBAL.sharedInstance, selector: #selector(reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: nil)
    }
    
    func startNotifier() {
        
        setupReachability("google.com")
        
        print("--- start notifier")
        do {
            try GLOBAL.sharedInstance.reachability?.startNotifier()
        } catch {
            print("Unable to create Reachability")
            return
        }
    }
    
    func stopNotifier() {
        print("--- stop notifier")
        GLOBAL.sharedInstance.reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: ReachabilityChangedNotification, object: nil)
        GLOBAL.sharedInstance.reachability = nil
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            GLOBAL.sharedInstance.isInternetReachable = true
        } else {
            GLOBAL.sharedInstance.isInternetReachable = false
        }
    }
}
func getFromNSUserDefaults(pForKey: String)-> String{
    var pReturnObject:String
    let defaults = UserDefaults.standard
    pReturnObject = defaults.object(forKey: pForKey) as? String ?? ""
    return pReturnObject;
}

func removeSpaceFromUrl(_ strImage:String) -> URL{
    let strImgUrl = strImage.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    let fileUrl = URL(fileURLWithPath: strImgUrl ?? "")
    
    return fileUrl
}
class indicatorHeader: NSObject {
    
    static let shared = indicatorHeader()
    
    var viewIndicator = DSGradientProgressView()

    func showIndicator(){
        viewIndicator.removeFromSuperview()
        viewIndicator = DSGradientProgressView()
//        if Constants.DeviceType.Is_IPhone_X || Constants.DeviceType.iPhoneXRMax{
//            viewIndicator.frame = CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width, height: 4)
//        }else{
            viewIndicator.frame = CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width, height: 4)
//        }
        viewIndicator.barColor = UIColor(red: 249/255.0, green: 147/255.0, blue: 160/255.0, alpha: 1.0)
        viewIndicator.signal()
        viewIndicator.wait()
        AppDelegate.sharedInstance().rootView().addSubview(viewIndicator)
    }
    func hideIndicator(){
        viewIndicator.removeFromSuperview()
    }
}

class indicator: NSObject {
    
    static let shared = indicator()
    
    private var progressObject: Progress?
    private var isSuccess: Bool?
    
    var backView = UIView()
    var indicatorView = UIView()

    func showIndicator(){
        if ARSLineProgress.shown { return }

        backView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        backView.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)


        AppDelegate.sharedInstance().rootView().addSubview(backView)

        ARSLineProgressConfiguration.showSuccessCheckmark = false

        progressObject = Progress(totalUnitCount: 1200)
        progressObject?.completedUnitCount = 110

        ARSLineProgress.showWithProgressObject(progressObject!, completionBlock: {
            ARSLineProgressConfiguration.restoreDefaults()
            print("Hidden with completion block - 1")
            self.backView.removeFromSuperview()
        })
        progressDemoHelper(success: true)
    }
    
    func hideIndicator(){
        self.backView.removeFromSuperview()

        ARSLineProgress.hideWithCompletionBlock {
            
        }
    }
    
    fileprivate func progressDemoHelper(success: Bool) {
        isSuccess = success
        ars_launchTimer()
    }
    
    fileprivate func ars_launchTimer() {
        let dispatchTime = DispatchTime.now() + Double(Int64(0.7 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC);
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.progressObject!.completedUnitCount += Int64(arc4random_uniform(30))
            if self.isSuccess == false && self.progressObject!.fractionCompleted >= 0.7 {
                ARSLineProgress.cancelProgressWithFailAnimation(true, completionBlock: {
                    print("Hidden with completion block")
                })
                return
            } else {
                if self.progressObject!.fractionCompleted >= 1.0 { return }
            }
            self.ars_launchTimer()
        })
    }
}

extension UICollectionView {
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 20)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel;
    }

    func restore() {
        self.backgroundView = nil
    }
}
extension UITableView {
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 20)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel;
    }

    func restore() {
        self.backgroundView = nil
    }
}
extension UIImage {
    func resized(withPercentage percentage: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    func resized(toWidth width: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}

extension UIImage {
    
    func crop(to:CGSize) -> UIImage {
        
        guard let cgimage = self.cgImage else { return self }
        
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        
        guard let newCgImage = contextImage.cgImage else { return self }
        
        let contextSize: CGSize = contextImage.size
        
        //Set to square
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        let cropAspect: CGFloat = to.width / to.height
        
        var cropWidth: CGFloat = to.width
        var cropHeight: CGFloat = to.height
        
        if to.width > to.height { //Landscape
            cropWidth = contextSize.width
            cropHeight = contextSize.width / cropAspect
            posY = (contextSize.height - cropHeight) / 2
        } else if to.width < to.height { //Portrait
            cropHeight = contextSize.height
            cropWidth = contextSize.height * cropAspect
            posX = (contextSize.width - cropWidth) / 2
        } else { //Square
            if contextSize.width >= contextSize.height { //Square on landscape (or square)
                cropHeight = contextSize.height
                cropWidth = contextSize.height * cropAspect
                posX = (contextSize.width - cropWidth) / 2
            }else{ //Square on portrait
                cropWidth = contextSize.width
                cropHeight = contextSize.width / cropAspect
                posY = (contextSize.height - cropHeight) / 2
            }
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)
        
        // Create bitmap image from context using the rect
        guard let imageRef: CGImage = newCgImage.cropping(to: rect) else { return self}
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        
        UIGraphicsBeginImageContextWithOptions(to, false, self.scale)
        cropped.draw(in: CGRect(x: 0, y: 0, width: to.width, height: to.height))
        let resized = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resized ?? self
    }
}
