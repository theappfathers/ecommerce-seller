//
//  APIManager.swift
//  Focus Fitness
//
//  Created by Theappfathers on 28/01/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import Foundation


struct APIManager {
      
    //Development
//    static let BASEURL                      =   "http://ecommerce.theappfathers.com/api/"
//    static let BASEURL                      =   "https://e-commerce.theappfathers.com/api/"
    static let BASEURL                      =   "https://zeilauk.com/api/"

        
    static let LOGIN_URL                    =   "login"
    static let REGISTRATION_URL             =   "register"
    static let FORGOT_PASSWORD              =   "forgot_password"
    static let RESET_PASSWORD               =   "reset_password"

    static let NOTIFICATION_URL             =   "notification"
    static let LOGO_UPDATE_URL              =   "logo_update"
    static let CATEGORY_URL                 =   "category"
    static let EXTRA_URL                    =   "extra"
    static let COLOR_URL                    =   "color"
    static let GET_DASHBOARD_URL            =   "seller_home"
    static let GET_ORDERS_URL               =   "order/history"
    static let ORDERS_STATUS_URL            =   ""
    static let GET_TYPE_CATEGORY            =   "type/product"

    static let GET_PRODUCTLIST_URL          =   "product"
    static let ADD_PRODUCT_URL              =   "product"
    static let ADD_STORY_URL                =   "story"
    static let REMOVE_PRODUCT_URL           =   "remove_product"


    static let SAVE_PROFILE_URL             =   "update_profile"
    static let GET_PROFILE_URL              =   "profile"

    static let GET_SHIPPING_ADD_URL         =   "shipping"
    static let SAVE_SHIPPING_ADDRESS_URL    =   "add_shipping"

    
    static let LOGOUT                       =   "logout"
}

struct API_Stripe {
    static let BASEURL                      =   "https://api.stripe.com/v1/"
    
    static let SOURCES                      =   "/sources"
    
    static let GET_CARD_LIST                =   "customers/"
    static let DELETE_CARD                  =   "customers/"
    
    static let CREATE_CARD_TOKEN            =   "tokens"
    
    static let POST_SUBSCRIPTIONS           =   "subscriptions"
    static let TEMPLATE_CHARGES             =   "charges"
    
}

let kAuthorization                          =   "Bearer sk_test_zMhNTJFnSh5JL5Fj8l4MfEAj00S3TuczIa"

struct StripeKey {
    static let kCustomer                    =   "customer"
}

func createUrlForStripeWebservice(url:String) -> String{
    
    let mainURL:String = API_Stripe.BASEURL+url
    
    return mainURL
}


func createUrlForWebservice(url:String) -> String{
    
    let mainURL:String = APIManager.BASEURL+url
    
    return mainURL
}

struct AlertTitle{
    
    static let App_Name                 = "Zeila Seller"
    static let Information              = "Information"
    static let Confirmation             = "Confirmation"
}

struct AlertMessages{
    static let kmsgNoInternet           =   "No Internet connection."
    static let kmsgServerError          =   "Internal Server Error."
    static let kmsgBlankCompanyName     =   "Please enter company name."
    static let kmsgBlankBrand           =   "Please enter brand."

    static let kmsgBlankPassword        =   "Please enter password."
    static let kmsgBlankEmail           =   "Please enter email address."
    static let kmsgValidEmail           =   "Please enter valid email address."
    static let kmsgPasswordNotMatch     =   "Your password and confirmation password do not match."
    static let kmsgConfirmPassword      =   "Please enter confirm password."
    static let kmsgTermsAndCondition    =   "Please select terms and condition."
    static let kmsgChooseFile           =   "Please upload your product image."

    static let kmsgBlankProductName     =   "Please enter product name."
    static let kmsgBlankProductTitle    =   "Please enter product title."
    static let kmsgBlankProductCode    =   "Please enter product Code."
    static let kmsgBlankProductPrice    =   "Please enter product price."
    static let kmsgBlankProductDesc    =   "Please enter product description."
    static let kmsgBlankProductImage    =   "Please select product Image."
    static let kmsgBlankProductColor    =   "Please select product Color."
    static let kmsgBlankProductSize    =   "Please select size."
    static let kmsgBlankProductType    =   "Please select product type."
    static let kmsgBlankProductCate    =   "Please select product category."
    static let kmsgBlankProductSubCate =   "Please select product sub category."
    
    static let kmsgBlankUsername        =   "Please enter name."

    static let kmsgBlankAddress         =   "Please enter address."
    static let kmsgBlankPostcode        =   "Please enter postcode."
    static let kmsgBlankCountry         =   "Please select country."
    static let kmsgBlanTown             =   "Please enter city/town."
    static let kmsgBlanPhoneNumber      =   "Please enter phone number."

}
