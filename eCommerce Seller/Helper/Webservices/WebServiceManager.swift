//
//  WebServiceManager.swift
//  SeasonLife
//
//  Created by Theappfathers on 26/11/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit
import Alamofire
import Foundation

typealias requestCompletionHandler = (NSDictionary) -> Void

typealias requestCompletionErrorHandler = (NSError) -> Void

class WebServiceManager: NSObject
{
    static let WebService = WebServiceManager()
    
    func sendRequestWithURL(_ URL: String,method: HTTPMethod,
                            queryParameters: [String: String]?,
                            bodyParameters: [String: Any]?,
                            headers: [String: String]?,
                            encodeType: ParameterEncoding = JSONEncoding.default,
                            completionHandler:@escaping requestCompletionHandler, failerHandler:@escaping requestCompletionErrorHandler) {
        
        // If there's a querystring, append it to the URL.
        
        if (GLOBAL.sharedInstance.isInternetReachable == false) {
            let userInfo: [NSObject : AnyObject] =
                [
                    NSLocalizedDescriptionKey as NSObject :  NSLocalizedString("No Internet", value: "No Internet Connection is there.", comment: "") as AnyObject,
                    NSLocalizedFailureReasonErrorKey as NSObject : NSLocalizedString("No Internet", value: "No Internet Connection is there.", comment: "") as AnyObject
            ]
            
            let error : NSError = NSError(domain: "EnomjiHttpResponseErrorDomain", code: -1, userInfo: (userInfo as! [String : Any]))
            
            failerHandler(error)
            print(error)
            return
        }
        
        let actualURL: String
        if let queryParameters = queryParameters {
            var components = URLComponents(string:URL)!
            components.queryItems = queryParameters.map { (key, value) in URLQueryItem(name: key, value: value) }
            actualURL = components.url!.absoluteString
        } else {
            actualURL = URL
        }
        
        var headerParams = [String: String]()
        if let headers = headers {
            headerParams = headers
        }
        
        print("Actual URL \(actualURL)")
        
        
        Alamofire.request(actualURL, method:method, parameters: bodyParameters,encoding:encodeType, headers: headerParams)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                
                switch response.result {
                case .success:
                    
                    if let result = response.result.value {
                        let JSON = result as? NSDictionary ?? [:]
                        print("JSON: \(JSON)")
                        
                        let wrappedResponse = NSDictionary.init(dictionary:JSON)
                        
                        DispatchQueue.main.async(execute: {
                            completionHandler(wrappedResponse)
                        })
                    }
                case .failure(let error):
                    let error = error
                    failerHandler(error as NSError)
                    print(error)
                }
        }
    }
    
    func sendRequestWithMultipartDataURL(_ URL: String,
                                         method: HTTPMethod,
                                         imgProfile: UIImage?,
                                         imgName: String?,
                                         queryParameters: [String: String]?,
                                         bodyParameters: [String: Any]?,
                                         headers: [String: String]?,
                                         completionHandler:@escaping requestCompletionHandler, failerHandler:@escaping requestCompletionErrorHandler) {
        
        // If there's a querystring, append it to the URL.
        
        if (GLOBAL.sharedInstance.isInternetReachable == false) {
            let userInfo: [NSObject : AnyObject] =
                [
                    NSLocalizedDescriptionKey as NSObject :  NSLocalizedString("No Internet", value: "No Internet Connection is there.", comment: "") as AnyObject,
                    NSLocalizedFailureReasonErrorKey as NSObject : NSLocalizedString("No Internet", value: "No Internet Connection is there.", comment: "") as AnyObject
            ]
            
            
            let error : NSError = NSError(domain: "EnomjiHttpResponseErrorDomain", code: -1, userInfo: (userInfo as! [String : Any]))
            
            failerHandler(error)
            print(error)
            return
        }
        
        let actualURL: String
        if let queryParameters = queryParameters {
            var components = URLComponents(string:URL)!
            components.queryItems = queryParameters.map { (key, value) in URLQueryItem(name: key, value: value) }
            actualURL = components.url!.absoluteString
        } else {
            actualURL = URL
        }
        
        var headerParams = [String: String]()
        if let headers = headers {
            headerParams = headers
        }
        print("Actual URL \(actualURL)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            let timestamp = Date().timeIntervalSince1970
            
            if let imageData = imgProfile?.pngData() {
                multipartFormData.append(imageData, withName: imgName ?? "", fileName: "\(timestamp).png", mimeType: "image/png")
            }
            
            for (key, value) in bodyParameters! {
                let paramValue = value as! String
                multipartFormData.append(paramValue.data(using: String.Encoding.utf8)!, withName: key)
            }}, to: actualURL, method: .post, headers: headerParams,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            print(response.request!)  // original URL request
                            print(response.result)   // result of response serialization
                            
                            switch response.result {
                            case .success:
                                if let result = response.result.value {
                                    let JSON = result as! NSDictionary
                                    print("JSON: \(JSON)")
                                    let wrappedResponse = NSDictionary.init(dictionary:JSON)
                                    
                                    DispatchQueue.main.async(execute: {
                                        completionHandler(wrappedResponse)
                                    })
                                }
                            case .failure(let error):
                                let error = error
                                failerHandler(error as NSError)
                                print(error)
                            }
                        }
                    case .failure(let encodingError):
                        let error = encodingError
                        failerHandler(error as NSError)
                        print(error)
                    }
        })
        
    }
    func sendRequestWithMultipartDataURL(_ URL: String,
                                         method: HTTPMethod,
                                         image: UIImage?,
                                         videoData: Data?,
                                         videoFileName : String?,
                                         fileName : String?,
                                         queryParameters: [String: String]?,
                                         bodyParameters: [String: Any]?,
                                         headers: [String: String]?,
                                         completionHandler:@escaping requestCompletionHandler, failerHandler:@escaping requestCompletionErrorHandler) {
        
        // If there's a querystring, append it to the URL.
        
        if (GLOBAL.sharedInstance.isInternetReachable == false) {
            let userInfo: [NSObject : AnyObject] =
                [
                    NSLocalizedDescriptionKey as NSObject : NSLocalizedString("No Internet", value: "No Internet Connection is there.", comment: "") as AnyObject,
                    NSLocalizedFailureReasonErrorKey as NSObject : NSLocalizedString("No Internet", value: "No Internet Connection is there.", comment: "") as AnyObject
            ]
            
            
            let error : NSError = NSError(domain: "EnomjiHttpResponseErrorDomain", code: -1, userInfo: (userInfo as! [String : Any]))
            
            failerHandler(error)
            print(error)
            return
        }
        
        let actualURL: String
        if let queryParameters = queryParameters {
            var components = URLComponents(string:URL)!
            components.queryItems = queryParameters.map { (key, value) in URLQueryItem(name: key, value: value) }
            actualURL = components.url!.absoluteString
        } else {
            actualURL = URL
        }
        
        var headerParams = [String: String]()
        if let headers = headers {
            headerParams = headers
        }
        print("Actual URL \(actualURL)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            if let fileName = fileName {
                
                let fileUrl = NSURL(string: fileName)
                
                if fileUrl?.pathExtension?.lowercased() == "doc" || fileUrl?.pathExtension?.lowercased() == "docx" {
                    let timestamp = Date().timeIntervalSince1970
                    
                    if let imageData = NSData(contentsOf: fileUrl! as URL){
                        multipartFormData.append(imageData as Data, withName: "resume_file", fileName: "\(timestamp).docx", mimeType: "application/doc")
                    }
                }else if fileUrl?.pathExtension?.lowercased() == "pdf"{
                    let timestamp = Date().timeIntervalSince1970
                    
                    if let imageData = NSData(contentsOf: fileUrl! as URL){
                        multipartFormData.append(imageData as Data, withName: "resume_file", fileName: "\(timestamp).pdf", mimeType: "application/pdf")
                    }
                }else {
                    
                }
            }
            
            if (image != nil)
            {
                let timestamp = Date().timeIntervalSince1970
                
                if let imageData = image?.jpegData(compressionQuality: 1.0) {
                    multipartFormData.append(imageData, withName: "profile_image", fileName: "\(timestamp).jpg", mimeType: "image/jpg")
                }
            }
            
            
            if let vFileName = videoFileName{
                let fileUrl = NSURL(string: vFileName)!
                let timestamp = Date().timeIntervalSince1970
                
                if fileUrl.pathExtension?.lowercased() == "mp4" || fileUrl.pathExtension?.lowercased() == "mp4" {
                    if let vData = videoData {
                        multipartFormData.append(vData, withName: "elevator_pitch_video", fileName: "\(timestamp).mp4", mimeType: "video/mp4")
                    }
                }
            }
            
            
            for (key, value) in bodyParameters! {
                let paramValue = value as? String
                multipartFormData.append((paramValue?.data(using: String.Encoding.utf8))!, withName: key)
            }}, to: actualURL, method: .post, headers: headerParams,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            print(response.request!) // original URL request
                            print(response.result) // result of response serialization
                            
                            switch response.result {
                            case .success:
                                if let result = response.result.value {
                                    let JSON = result as? NSDictionary ?? [:]
                                    print("JSON: \(JSON)")
                                    let wrappedResponse = NSDictionary.init(dictionary:JSON)
                                    
                                    DispatchQueue.main.async(execute: {
                                        completionHandler(wrappedResponse)
                                    })
                                }
                            case .failure(let error):
                                let error = error
                                failerHandler(error as NSError)
                                print(error)
                            }
                        }
                    case .failure(let encodingError):
                        let error = encodingError
                        failerHandler(error as NSError)
                        print(error)
                    }
        })
        
    }
    
    func sendRequestInterviewWithMultipartDataURL(_ URL: String,
                                                  method: HTTPMethod,
                                                  image: UIImage?,
                                                  videoData: Data?,
                                                  videoFileName : String?,
                                                  fileName : String?,
                                                  queryParameters: [String: String]?,
                                                  bodyParameters: [String: Any]?,
                                                  headers: [String: String]?,
                                                  completionHandler:@escaping requestCompletionHandler, failerHandler:@escaping requestCompletionErrorHandler) {
        
        // If there's a querystring, append it to the URL.
        
        if (GLOBAL.sharedInstance.isInternetReachable == false) {
            let userInfo: [NSObject : AnyObject] =
                [
                    NSLocalizedDescriptionKey as NSObject : NSLocalizedString("No Internet", value: "No Internet Connection is there.", comment: "") as AnyObject,
                    NSLocalizedFailureReasonErrorKey as NSObject : NSLocalizedString("No Internet", value: "No Internet Connection is there.", comment: "") as AnyObject
            ]
            
            
            let error : NSError = NSError(domain: "EnomjiHttpResponseErrorDomain", code: -1, userInfo: (userInfo as! [String : Any]))
            
            failerHandler(error)
            print(error)
            return
        }
        
        let actualURL: String
        if let queryParameters = queryParameters {
            var components = URLComponents(string:URL)!
            components.queryItems = queryParameters.map { (key, value) in URLQueryItem(name: key, value: value) }
            actualURL = components.url!.absoluteString
        } else {
            actualURL = URL
        }
        
        var headerParams = [String: String]()
        if let headers = headers {
            headerParams = headers
        }
        print("Actual URL \(actualURL)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            if let vFileName = videoFileName{
                let fileUrl = NSURL(string: vFileName)!
                let timestamp = Date().timeIntervalSince1970
                
                if fileUrl.pathExtension?.lowercased() == "mp4" || fileUrl.pathExtension?.lowercased() == "mp4" {
                    if let vData = videoData {
                        multipartFormData.append(vData, withName: "interview_video", fileName: "\(timestamp).mp4", mimeType: "video/mp4")
                    }
                }
            }
            
            
            for (key, value) in bodyParameters! {
                let paramValue = value as? String
                multipartFormData.append((paramValue?.data(using: String.Encoding.utf8))!, withName: key)
            }}, to: actualURL, method: .post, headers: headerParams,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            print(response.request!) // original URL request
                            print(response.result) // result of response serialization
                            
                            switch response.result {
                            case .success:
                                if let result = response.result.value {
                                    let JSON = result as? NSDictionary ?? [:]
                                    print("JSON: \(JSON)")
                                    let wrappedResponse = NSDictionary.init(dictionary:JSON)
                                    
                                    DispatchQueue.main.async(execute: {
                                        completionHandler(wrappedResponse)
                                    })
                                }
                            case .failure(let error):
                                let error = error
                                failerHandler(error as NSError)
                                print(error)
                            }
                        }
                    case .failure(let encodingError):
                        let error = encodingError
                        failerHandler(error as NSError)
                        print(error)
                    }
        })
        
    }
    func sendRequestWithMultipartDataURLMulitiple(_ URL: String, method: HTTPMethod, arrImages: [UIImage]?, fileName : [String]?,
                                                  bodyParameters: [String: Any]?, headers: [String: String]?,
                                                  completionHandler:@escaping requestCompletionHandler, failerHandler:@escaping requestCompletionErrorHandler)
    {
        if (GLOBAL.sharedInstance.isInternetReachable == false) {
            let userInfo: [NSObject : AnyObject] =
                [
                    NSLocalizedDescriptionKey as NSObject : NSLocalizedString("No Internet", value: "No Internet Connection is there.", comment: "") as AnyObject,
                    NSLocalizedFailureReasonErrorKey as NSObject : NSLocalizedString("No Internet", value: "No Internet Connection is there.", comment: "") as AnyObject
            ]
            
            
            let error : NSError = NSError(domain: "EnomjiHttpResponseErrorDomain", code: -1, userInfo: (userInfo as! [String : Any]))
            
            failerHandler(error)
            print(error)
            return
        }
        
        let actualURL: String
        actualURL = URL
        
        var headerParams = [String: String]()
        if let headers = headers {
            headerParams = headers
        }
        print("Actual URL \(actualURL)")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            if arrImages!.count > 0 {
                for i in 0..<arrImages!.count
                {
                    let image = arrImages?[i]
                    if let imageData = image?.pngData() {
                        multipartFormData.append(imageData, withName: fileName![i], fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")
                    }
                }
            }
            
            for (key, value) in bodyParameters! {
                let paramValue = value as? String
                multipartFormData.append((paramValue?.data(using: String.Encoding.utf8)!)!, withName: key)
            }}, to: actualURL, method: .post, headers: headerParams,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            print(response.request!) // original URL request
                            print(response.result) // result of response serialization
                            
                            switch response.result {
                            case .success:
                                if let result = response.result.value {
                                    let JSON = result as! NSDictionary
                                    print("JSON: \(JSON)")
                                    let wrappedResponse = NSDictionary.init(dictionary:JSON)
                                    
                                    DispatchQueue.main.async(execute: {
                                        completionHandler(wrappedResponse)
                                    })
                                }
                            case .failure(let error):
                                let error = error
                                failerHandler(error as NSError)
                                print(error)
                            }
                        }
                    case .failure(let encodingError):
                        let error = encodingError
                        failerHandler(error as NSError)
                        print(error)
                    }
        })
        
    }
}
