//
//  AppDelegate.swift
//  eCommerce Seller
//
//  Created by Theappfathers on 11/04/20.
//  Copyright © 2020 Theappfathers. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        GLOBAL().startNotifier()
        
        autoLogin()

        return true
    }
    //MARK:- AUTO LOGIN
    //MARK:-
    func pushToView(viewIdentifier:String){
        let vc1 = STORYBOARD.instantiateViewController(withIdentifier: viewIdentifier)
        let controllers:Array = [vc1]
        let navController:UINavigationController = window?.rootViewController as! UINavigationController
        navController.setViewControllers(controllers, animated: true)
    }
    func autoLogin(){
        if getBoolFromUserDefaults(pForKey: "session"){
            pushToView(viewIdentifier: "TabBarVC")
        }
    }
}

extension AppDelegate{
    class func sharedInstance() -> (AppDelegate){
        return UIApplication.shared.delegate as! AppDelegate
    }
    func rootViewController() -> UIViewController{
        if let viewCont = UIApplication.shared.keyWindow?.rootViewController{
            return viewCont
        }
        return UIViewController()
    }
    func rootView() -> UIView{
        return rootViewController().view
    }
    func classNameAsString(obj: Any) -> String {
        return "\(type(of: obj))"
    }
    func moveToRootVC(viewController:UIViewController){
        let identifire = classNameAsString(obj: viewController)
        if identifire != ""{
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewCont = mainStoryboard.instantiateViewController(withIdentifier: identifire)
            let navigationController = UINavigationController(rootViewController: viewCont)
            navigationController.setNavigationBarHidden(true, animated: false)
            self.window?.rootViewController = navigationController
        }
    }
}
